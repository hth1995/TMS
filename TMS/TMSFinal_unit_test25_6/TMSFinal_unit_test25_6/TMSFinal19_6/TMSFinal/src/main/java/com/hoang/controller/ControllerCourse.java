package com.hoang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.domain.Category;
import com.hoang.domain.Course;
import com.hoang.domain.User;
import com.hoang.service.ServiceCategory;
import com.hoang.service.ServiceCourse;
import com.hoang.service.ServiceTopic;
import com.hoang.validator.CategoryValidator;


@Controller
@RequestMapping(value="/formcourse")
public class ControllerCourse {
	
	@Autowired
	ServiceCategory category;
	@Autowired
	ServiceCourse course;
	@Autowired
	CategoryValidator categoryValidator;
	@Autowired
	ServiceTopic topic;
	 @InitBinder
	   protected void initBinder(WebDataBinder dataBinder) {
	   
	       // Form mục tiêu
	       Object target = dataBinder.getTarget();
	       if (target == null) {
	           return;
	       }
	       System.out.println("Target=" + target);
	 
	       if (target.getClass() == User.class) {
	           dataBinder.setValidator(categoryValidator);
	       }
	   }
	@RequestMapping(value="/themcourse")
	public String them(Model model){
		model.addAttribute("course",new Course());
		model.addAttribute("category",new Category());
		model.addAttribute("listCategory",category.lietKe());
		model.addAttribute("listCourse",course.lietKe());
		return "themcourse";
	}
	
//	@RequestMapping(value="/xulythem")
//	public String xulythem(@ModelAttribute Course ct){
//		course.them(ct);
//		return "redirect:/formcourse/themcourse";
//	}
	
	@RequestMapping(value="/xulythem",method=RequestMethod.POST)
	public String xulythem(Model model,@ModelAttribute("course") @Validated Course ct, BindingResult result, final RedirectAttributes redirectAttributes){
		//userValidator.validate(us, result);
		 if(result.hasErrors()){
			 return "themcourse";	
		 }
		 course.them(ct); 
		 return "redirect:/formcourse/themcourse";
		
	}
//	@RequestMapping(value="/xoacourse/{id}")
//	public String xoa(@PathVariable String id){
//		course.xoa(Integer.parseInt(id));
//		return "redirect:/formcourse/themcourse";
//	}
	@RequestMapping(value="/xoacourse/{id}")
	public String xoa(@PathVariable String id){
		try{
		course.xoa(Integer.parseInt(id));
		}
		catch (Exception e)
		{
			return "403";
		}
		return "redirect:/formcourse/themcourse";
	}
	
	@RequestMapping(value="/suacourse/{id}")
	public String sua(@PathVariable String id,Model model){
		model.addAttribute("course",course.findOneSV(Integer.parseInt(id)));
		model.addAttribute("listCategory",category.lietKe());
		model.addAttribute("listCourse",course.lietKe());
		return "suacourse";
	}
	@RequestMapping(value="/xemtopic/{id}")
	public String xemtopic(@PathVariable String id,Model model){
		model.addAttribute("course",course.findOneSV(Integer.parseInt(id)));
		model.addAttribute("listCategory",category.lietKe());
		model.addAttribute("listCourse",course.lietKe());
		model.addAttribute("listTopic",topic.findAllByCourseCourseId(Integer.parseInt(id)));
		return "xemtopic";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Course cr){
		course.sua(cr);
		return "redirect:/formcourse/themcourse";
	}

	@RequestMapping(value="/xulysearch")
	public String xulysearch(@RequestParam("search") String search,Model model){
		
		if(search.equals(""))
		{
			return "redirect:/formcourse/themcourse";
		}
		model.addAttribute("course",new Course());
		model.addAttribute("category",new Category());
		model.addAttribute("listCategory",category.lietKe());
		model.addAttribute("listCourse",course.findAllByCourseNameOrDescription(search));
		return "themcourse";
	}
	
}
