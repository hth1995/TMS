package com.hoang.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hoang.domain.Property;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceProperty;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;

@Controller
@RequestMapping(value="/formtrainingpropertytrainer")
public class ControllerTrainingPropertyTrainer {
	@Autowired
	ServiceUserProperty userProperty;
	@Autowired
	ServiceUser user;
	@Autowired
	ServiceProperty property;
	@Autowired
	ServiceJob job;
	
	@RequestMapping(value="/training_addproperty_trainer")
	public String them(Model model){
		
		model.addAttribute("userPropertyId",new UserpropertyId());
		model.addAttribute("userProperty",new Userproperty());
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		model.addAttribute("listUserProperty",userProperty.findAllByUserJob(job.findByJobName("trainer")));
		List<Property> l1 = new ArrayList<Property>();
		l1.add(property.findByPropertyName("name"));
		l1.add(property.findByPropertyName("external"));
		l1.add(property.findByPropertyName("internal"));
		l1.add(property.findByPropertyName("working_place"));
		l1.add(property.findByPropertyName("telephone"));
		l1.add(property.findByPropertyName("email"));
		model.addAttribute("listProperty",l1);
		List<User> l = new ArrayList<User>();
		l = user.findAllByJob(job.findByJobName("trainer"));
		model.addAttribute("listUser",l);
//		session = request.getSession();
//		model.addAttribute("loggedInUser", session);
		return "training_addproperty_trainer";
	}
	
	@RequestMapping(value="/xulythem")
	public String xulythem(@ModelAttribute Userproperty u){
		userProperty.them(u);
		return "redirect:/formtrainingpropertytrainer/training_addproperty_trainer";
	}
	
	@RequestMapping(value="/xoauserproperty/{id1}/{id2}")
	public String xoa(@PathVariable String id1,@PathVariable String id2){
		userProperty.xoa(Integer.parseInt(id1),Integer.parseInt(id2));
		return "redirect:/formtrainingpropertytrainer/training_addproperty_trainer";
	}
	
	@RequestMapping(value="/training_editproperty_trainer/{id1}/{id2}")
	public String sua(@PathVariable String id1,@PathVariable String id2,Model model){
		model.addAttribute("userProperty",userProperty.findAllByIdPropertyIdAndIdUserId(Integer.parseInt(id1), Integer.parseInt(id2)));
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		model.addAttribute("listUserProperty",userProperty.findAllByUserJob(job.findByJobName("trainer")));
		List<Property> l1 = new ArrayList<Property>();
		l1.add(property.findByPropertyName("name"));
		l1.add(property.findByPropertyName("external"));
		l1.add(property.findByPropertyName("internal"));
		l1.add(property.findByPropertyName("working_place"));
		l1.add(property.findByPropertyName("telephone"));
		l1.add(property.findByPropertyName("email"));
		model.addAttribute("listProperty",l1);
		List<User> l = new ArrayList<User>();
		l = user.findAllByJob(job.findByJobName("trainer"));
		model.addAttribute("listUser",l);
		return "training_editproperty_trainer";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Userproperty u){
		userProperty.sua(u);
		return "redirect:/formtrainingpropertytrainer/training_addproperty_trainer";
	}
	
	
}
