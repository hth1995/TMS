package com.hoang.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.hoang.DAO.DAOJob;
import com.hoang.DAO.DAOUser;
import com.hoang.domain.Job;
import com.hoang.domain.User;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private DAOUser userRepository;
	
//    @Autowired
//    private UserRepository userRepository;

	@Autowired
	private DAOJob jobRepository;
	
//    @Autowired
//    private JobRepository jobRepository;

    @Autowired 
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
        // jobs
        if (jobRepository.findByJobName("admin") == null) {
        	Job temp = new Job();
        	temp.setJobName("admin");
            jobRepository.save(temp);
        }
        
        if (jobRepository.findByJobName("trainee") == null) {
        	Job temp = new Job();
        	temp.setJobName("trainee");
            jobRepository.save(temp);
        }
        
        if (jobRepository.findByJobName("training") == null) {
        	Job temp = new Job();
        	temp.setJobName("training");
            jobRepository.save(temp);
        }
        
        if (jobRepository.findByJobName("trainer") == null) {
        	Job temp = new Job();
        	temp.setJobName("trainer");
            jobRepository.save(temp);
        }

        // Admin account
        if (userRepository.findByAccount("admin") == null) {
            User admin = new User();
            admin.setAccount("admin");
            admin.setPassword(passwordEncoder.encode("admin"));
            admin.setJob(jobRepository.findByJobName("admin"));
            userRepository.save(admin);
        }
    
//
//        // Member account
        if (userRepository.findByAccount("member") == null) {
            User user = new User();
            user.setAccount("member");
            user.setPassword(passwordEncoder.encode("member"));
            user.setJob(jobRepository.findByJobName("training"));
            userRepository.save(user);
        }
      
    }

}