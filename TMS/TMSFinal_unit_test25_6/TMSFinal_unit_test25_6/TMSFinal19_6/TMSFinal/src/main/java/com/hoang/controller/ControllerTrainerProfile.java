package com.hoang.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hoang.domain.Property;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceProperty;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;

@Controller
@RequestMapping(value="/formtrainerprofile")
public class ControllerTrainerProfile {
	@Autowired
	ServiceUserProperty userProperty;
	@Autowired
	ServiceUser user;
	@Autowired
	ServiceProperty property;
	@Autowired
	ServiceJob job;
	
	@RequestMapping(value="/trainer_addprofile")
	public String them(Model model){
		
		model.addAttribute("userPropertyId",new UserpropertyId());
		model.addAttribute("userProperty",new Userproperty());
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		String x = loggedInUser.getName();
		model.addAttribute("listUserProperty",userProperty.findAllByUserAccount(x));
		List<Property> l1 = new ArrayList<Property>();
		l1.add(property.findByPropertyName("name"));
		l1.add(property.findByPropertyName("external"));
		l1.add(property.findByPropertyName("internal"));
		l1.add(property.findByPropertyName("working_place"));
		l1.add(property.findByPropertyName("telephone"));
		l1.add(property.findByPropertyName("email"));
		model.addAttribute("listProperty",l1);
		
		model.addAttribute("listUser",user.findByAccount(x));
//		session = request.getSession();
//		model.addAttribute("loggedInUser", session);
		return "trainer_addprofile";
	}
	
	@RequestMapping(value="/xulythem")
	public String xulythem(@ModelAttribute Userproperty u){
		userProperty.them(u);
		return "redirect:/formtrainerprofile/trainer_addprofile";
	}
	
	@RequestMapping(value="/xoauserproperty/{id1}/{id2}")
	public String xoa(@PathVariable String id1,@PathVariable String id2){
		userProperty.xoa(Integer.parseInt(id1),Integer.parseInt(id2));
		return "redirect:/formtrainerprofile/trainer_addprofile";
	}
	
	@RequestMapping(value="/trainer_editprofile/{id1}/{id2}")
	public String sua(@PathVariable String id1,@PathVariable String id2,Model model){
		model.addAttribute("userProperty",userProperty.findAllByIdPropertyIdAndIdUserId(Integer.parseInt(id1), Integer.parseInt(id2)));
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		String x = loggedInUser.getName();
		model.addAttribute("listUserProperty",userProperty.findAllByUserAccount(x));
		List<Property> l1 = new ArrayList<Property>();
		l1.add(property.findByPropertyName("name"));
		l1.add(property.findByPropertyName("external"));
		l1.add(property.findByPropertyName("internal"));
		l1.add(property.findByPropertyName("working_place"));
		l1.add(property.findByPropertyName("telephone"));
		l1.add(property.findByPropertyName("email"));
		model.addAttribute("listProperty",l1);
		
		model.addAttribute("listUser",user.findByAccount(x));
		return "trainer_editprofile";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Userproperty u){
		userProperty.sua(u);
		return "redirect:/formtrainerprofile/trainer_addprofile";
	}
	
	
}
