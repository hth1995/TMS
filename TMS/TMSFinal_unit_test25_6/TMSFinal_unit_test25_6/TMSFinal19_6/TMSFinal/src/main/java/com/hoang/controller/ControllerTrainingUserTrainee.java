package com.hoang.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.domain.Job;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;
import com.hoang.validator.UserValidator;

@Controller
@RequestMapping(value="/formtraining")
public class ControllerTrainingUserTrainee {

	@Autowired
	ServiceJob job;
	@Autowired
	ServiceUser user;
	@Autowired
	UserValidator userValidator;
	@Autowired
	ServiceUserProperty userpro;
	
	 @InitBinder
	   protected void initBinder(WebDataBinder dataBinder) {
	   
	       // Form mục tiêu
	       Object target = dataBinder.getTarget();
	       if (target == null) {
	           return;
	       }
	       System.out.println("Target=" + target);
	 
	       if (target.getClass() == User.class) {
	           dataBinder.setValidator(userValidator);
	       }
	   }
	
	
	@RequestMapping(value="/training_adduser_trainee")
	public String them(Model model){
		
		model.addAttribute("user",new User());
		model.addAttribute("job",new Job());
		Job temp = job.findByJobName("trainee");
		List<Job> l = new ArrayList<Job>();
		l.add(temp);
		model.addAttribute("listJob",l);
		List<User> l2 = new ArrayList<User>();
		l2=user.findAllByJob(temp);
		model.addAttribute("listUser",l2);
		return "training_adduser_trainee";
	}
	
//	@RequestMapping(value="/xulythem")
//	public String xulythem(@ModelAttribute User u){
//		user.them(u);
//		return "redirect:/formtraining/training_adduser_trainee";
//	}
	
	@RequestMapping(value="/xulythem",method=RequestMethod.POST)
	public String xulythem(Model model,@ModelAttribute("user") @Validated User us, BindingResult result, final RedirectAttributes redirectAttributes){
		//userValidator.validate(us, result);
		 if(result.hasErrors()){
			 return "training_adduser_trainee";	
		 }
		 user.them(us); 
		 return "redirect:/formtraining/training_adduser_trainee";
		
	}
	
//	@RequestMapping(value="/xoauser/{id}")
//	public String xoa(@PathVariable String id){
//		user.xoa(Integer.parseInt(id));
//		return "redirect:/formtraining/training_adduser_trainee";
//	}
	@RequestMapping(value="/xoauser/{id}")
	public String xoa(@PathVariable String id){
		User a= user.findOneSV(Integer.parseInt(id));
		List <Userproperty> l =userpro.findAllByUser(a);
		for(int i=0; i<l.size();i++)
		{
			userpro.delete(l.get(i));
		}
		user.xoa(Integer.parseInt(id));
		return "redirect:/formtraining/training_adduser_trainee";
	}
	
	@RequestMapping(value="/training_edituser_trainee/{id}")
	public String sua(@PathVariable String id,Model model){
		
		model.addAttribute("user",user.findOneSV(Integer.parseInt(id)));
		model.addAttribute("job", new Job());
		List<Job> l = new ArrayList<Job>();
		l.add(job.findByJobName("trainee"));
		model.addAttribute("listJob",l);
		model.addAttribute("listUser",user.findAllByJob(job.findByJobName("trainee")));
		return "training_edituser_trainee";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute User u){
		user.sua(u);
		return "redirect:/formtraining/training_adduser_trainee";
	}
	
	@RequestMapping(value="/xulylogout")
	public String logout(HttpSession session){
		session.removeAttribute("loggedInUser");
		return "redirect:/formlogin/login";
	}
}

