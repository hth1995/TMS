package com.hoang.domain;

public class ClassTraineeProfile {

	private String account;
	private int userid;
	private String age;
	private String name;	
	private String date_of_birth;
	private String education;
	private String main_program_language;
	private String toeic_score;
	private String experience;
	private String department;
	private String location;
	public ClassTraineeProfile() {
		super();
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getMain_program_language() {
		return main_program_language;
	}
	public void setMain_program_language(String main_program_language) {
		this.main_program_language = main_program_language;
	}
	public String getToeic_score() {
		return toeic_score;
	}
	public void setToeic_score(String toeic_score) {
		this.toeic_score = toeic_score;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
}
