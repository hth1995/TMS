package com.hoang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hoang.domain.Job;
import com.hoang.service.ServiceJob;

@Controller
@RequestMapping(value="/formjob")
public class ControllerJob {
	
	@Autowired
	ServiceJob job;
	
	@RequestMapping(value="/themjob")
	public String them(Model model){
		model.addAttribute("job",new Job());
		model.addAttribute("listJob",job.lietKe());
		return "themjob";
	}
	
	@RequestMapping(value="/xulythem")
	public String xulythem(@ModelAttribute Job j){
		job.them(j);
		return "redirect:/formjob/themjob";
	}
	
	@RequestMapping(value="/xoajob/{id}")
	public String xoa(@PathVariable String id){
		job.xoa(Integer.parseInt(id));
		return "redirect:/formjob/themjob";
	}
	
	@RequestMapping(value="/suajob/{id}")
	public String sua(@PathVariable String id,Model model){
		model.addAttribute("job",job.findOneSV(Integer.parseInt(id)));
		model.addAttribute("listJob",job.lietKe());
		return "suajob";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Job j){
		job.sua(j);
		return "redirect:/formjob/themjob";
	}


}
