package com.hoang.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hoang.domain.Course;
import com.hoang.domain.Topic;

@Transactional

public interface DAOCourse extends JpaRepository<Course, Integer>{
	
	Course findByCourseName(String courseName);
	
	@Query("SELECT c FROM Course c where c.courseName like %:search% OR c.description like %:search%")
	List<Course> findAllByCourseNameOrDescription(@Param("search") String search);
	List <Course> findAllByTopics (Topic topics);
}
