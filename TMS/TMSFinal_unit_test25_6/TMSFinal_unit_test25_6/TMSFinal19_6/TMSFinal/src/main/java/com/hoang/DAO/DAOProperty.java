package com.hoang.DAO;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hoang.domain.Property;

@Transactional
public interface DAOProperty extends JpaRepository<Property, Integer> {
	Property findByPropertyName (String propertyName);
	Property findByPropertyId (Integer id);
}
