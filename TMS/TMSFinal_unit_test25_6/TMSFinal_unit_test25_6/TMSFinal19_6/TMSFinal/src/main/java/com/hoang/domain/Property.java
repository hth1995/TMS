package com.hoang.domain;
// Generated May 26, 2017 3:32:03 PM by Hibernate Tools 5.2.3.Final

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Property generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "property", catalog = "tms2")
public class Property implements java.io.Serializable {

	private Integer propertyId;
	private String propertyName;
	private String description;
	private Set<Userproperty> userproperties = new HashSet<Userproperty>(0);

	public Property() {
	}

	public Property(String propertyName, String description, Set<Userproperty> userproperties) {
		this.propertyName = propertyName;
		this.description = description;
		this.userproperties = userproperties;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "PropertyID", unique = true, nullable = false)
	public Integer getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
@NotEmpty
	@Column(name = "PropertyName", length = 45)
	public String getPropertyName() {
		return this.propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	@Column(name = "Description", length = 45)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "property")
	public Set<Userproperty> getUserproperties() {
		return this.userproperties;
	}

	public void setUserproperties(Set<Userproperty> userproperties) {
		this.userproperties = userproperties;
	}

}
