package com.hoang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.domain.Permission;
import com.hoang.domain.User;
import com.hoang.service.ServicePermission;
import com.hoang.validator.PermissionValidator;

@Controller
@RequestMapping(value="/formpermission")
public class ControllerPermission {
	
	@Autowired
	ServicePermission permission;
	@Autowired
	PermissionValidator permissionValidator;
	
	 @InitBinder
	   protected void initBinder(WebDataBinder dataBinder) {
	   
	       // Form mục tiêu
	       Object target = dataBinder.getTarget();
	       if (target == null) {
	           return;
	       }
	       System.out.println("Target=" + target);
	 
	       if (target.getClass() == User.class) {
	           dataBinder.setValidator(permissionValidator);
	       }
	   }
	
	@RequestMapping(value="/thempermission")
	public String them(Model model){
		model.addAttribute("permission",new Permission());
		model.addAttribute("listPermission",permission.lietKe());
		return "thempermission";
	}
	
//	@RequestMapping(value="/xulythem")
//	public String xulythem(@ModelAttribute Permission pm){
//		permission.them(pm);
//		return "redirect:/formpermission/thempermission";
//	}
	
	@RequestMapping(value="/xulythem",method=RequestMethod.POST)
	public String xulythem(Model model,@ModelAttribute("permission") @Validated Permission pm, BindingResult result, final RedirectAttributes redirectAttributes){
		//userValidator.validate(us, result);
		 if(result.hasErrors()){
			 return "thempermission";	
		 }
		 permission.them(pm); 
		 return "redirect:/formpermission/thempermission";
		
	}
	@RequestMapping(value="/xoapermission/{id}")
	public String xoa(@PathVariable String id){
		permission.xoa(Integer.parseInt(id));
		return "redirect:/formpermission/thempermission";
	}
	
	@RequestMapping(value="/suapermission/{id}")
	public String sua(@PathVariable String id,Model model){
		model.addAttribute("permission",permission.findOneSV(Integer.parseInt(id)));
		model.addAttribute("listPermission",permission.lietKe());
		return "suapermission";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Permission pm){
		permission.sua(pm);
		return "redirect:/formpermission/thempermission";
	}

}
