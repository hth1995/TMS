package com.hoang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hoang.domain.Topic;
import com.hoang.service.ServiceTopic;

@Controller
@RequestMapping(value="/formtrainerviewcourse")
public class ControllerTrainerViewCourse {
	
	@Autowired
	ServiceTopic topic;
	
	@RequestMapping(value="/trainer_viewcourse")
	public String them(Model model){
		
		model.addAttribute("topic",new Topic());
		Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		String x = loggedInUser.getName();
		
		model.addAttribute("listTopic",topic.findAllByUserAccount(x));
		return "trainer_viewcourse";
	}
	}