package com.hoang.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hoang.domain.Course;
import com.hoang.domain.CourseUser;
import com.hoang.domain.CourseUserId;
import com.hoang.domain.User;
import com.hoang.service.ServiceCourse;
import com.hoang.service.ServiceCourseUser;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceUser;

@Controller
@RequestMapping(value="/formcourseuser")
public class ControllerTraniningCourseUser{
	@Autowired
	ServiceCourseUser courseUser;
	@Autowired
	ServiceUser user;
	@Autowired
	ServiceCourse course;
	@Autowired
	ServiceJob job;
	
	@RequestMapping(value="/training_addcourse_trainee")
	public String them(Model model){
		
		model.addAttribute("courseUserId",new CourseUserId());
		model.addAttribute("courseUser",new CourseUser());
		model.addAttribute("user",new User());
		model.addAttribute("course",new Course());
		model.addAttribute("listCourseUser",courseUser.lietKe());
		model.addAttribute("listCourse",course.lietKe());
		List<User> l = new ArrayList<User>();
		l = user.findAllByJob(job.findByJobName("trainee"));
		model.addAttribute("listUser",l);
//		session = request.getSession();
//		model.addAttribute("loggedInUser", session);
		return "training_addcourse_trainee";
	}
	
	@RequestMapping(value="/xulythem")
	public String xulythem(@ModelAttribute CourseUser u){
		courseUser.them(u);
		return "redirect:/formcourseuser/training_addcourse_trainee";
	}
	
	@RequestMapping(value="/xoaCourseUser/{id1}/{id2}")
	public String xoa(@PathVariable String id1,@PathVariable String id2){
		courseUser.xoa(Integer.parseInt(id1),Integer.parseInt(id2));
		return "redirect:/formcourseuser/training_addcourse_trainee";
	}
}
	
