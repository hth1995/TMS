package com.hoang.domain;

public class ClassTrainerProfile {

	private String account;
	private int userid;
	private String name;
	private String external;
	private String internal;
	private String working_place;
	private String telephone;
	private String email;
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExternal() {
		return external;
	}
	public void setExternal(String external) {
		this.external = external;
	}
	public String getInternal() {
		return internal;
	}
	public void setInternal(String internal) {
		this.internal = internal;
	}
	public String getWorking_place() {
		return working_place;
	}
	public void setWorking_place(String working_place) {
		this.working_place = working_place;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ClassTrainerProfile(String account, int userid, String name, String external, String internal,
			String working_place, String telephone, String email) {
		super();
		this.account = account;
		this.userid = userid;
		this.name = name;
		this.external = external;
		this.internal = internal;
		this.working_place = working_place;
		this.telephone = telephone;
		this.email = email;
	}
	public ClassTrainerProfile() {
		super();
	}
	
}
