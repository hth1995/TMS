package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOCourse;
import com.hoang.domain.Course;
import com.hoang.domain.Topic;


@Service

public class ServiceCourse {
	@Autowired
	DAOCourse service;
	
	public void them(Course cs){
		if(service.findByCourseName(cs.getCourseName()) == null)
		service.save(cs);
	}
	
	public void xoa(int id){
		service.delete(id);
	}
	
	public Course findOneSV(int id){
		return service.findOne(id);
	}
	
	public void sua(Course cr){
		Course temp = service.findOne(cr.getCourseId());
		temp.setCourseId(cr.getCourseId());
		temp.setCourseName(cr.getCourseName());
		temp.setDescription(cr.getDescription());
		temp.setCategory(cr.getCategory());
		service.save(temp);
	}
	
	public List<Course> lietKe(){
		return service.findAll();
	}
	
	public List<Course> findAllByCourseNameOrDescription(String search){
		return service.findAllByCourseNameOrDescription(search);
	}
	
	public List<Course> findbytopic(Topic a){
		return service.findAllByTopics(a);
	}
}
