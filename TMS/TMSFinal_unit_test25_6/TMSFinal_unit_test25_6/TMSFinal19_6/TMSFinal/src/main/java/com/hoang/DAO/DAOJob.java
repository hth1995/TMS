package com.hoang.DAO;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hoang.domain.Job;

@Transactional
public interface DAOJob extends JpaRepository<Job, Integer> {

	Job findByJobName(String jobName);
}
