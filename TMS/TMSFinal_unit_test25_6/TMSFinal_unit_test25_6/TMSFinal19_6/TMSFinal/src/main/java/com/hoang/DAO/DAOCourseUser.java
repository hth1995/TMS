package com.hoang.DAO;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hoang.domain.CourseUser;
import com.hoang.domain.CourseUserId;

@Transactional
public interface DAOCourseUser extends JpaRepository<CourseUser, CourseUserId>{

	CourseUser findAllByIdCourseIdAndIdUserId(Integer idCourseId,Integer idUserId);

}
