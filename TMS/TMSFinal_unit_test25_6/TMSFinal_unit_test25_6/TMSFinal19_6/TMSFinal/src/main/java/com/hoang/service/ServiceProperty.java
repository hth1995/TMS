package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOProperty;
import com.hoang.domain.Property;

@Service
public class ServiceProperty {
	
	@Autowired
	DAOProperty service;
	
	public void them(Property pp){
		if(service.findByPropertyName(pp.getPropertyName()) == null)
		service.save(pp);
	}
	
	public void xoa(int id){
		service.delete(id);
	}
	
	public Property findOneSV(int id){
		return service.findByPropertyId(id);
	}
	
	public Property findbyid(int id){
		return service.findByPropertyId(id);
	}
	public void sua(Property pp){
		Property temp = service.findOne(pp.getPropertyId());
		temp.setPropertyId(pp.getPropertyId());
		temp.setPropertyName(pp.getPropertyName());
		temp.setDescription(pp.getDescription());
		service.save(temp);
	}

	public List<Property> lietKe(){
		return service.findAll();
	}
	
	public Property findByPropertyName(String propertyName){
		return service.findByPropertyName(propertyName);
	}

}
