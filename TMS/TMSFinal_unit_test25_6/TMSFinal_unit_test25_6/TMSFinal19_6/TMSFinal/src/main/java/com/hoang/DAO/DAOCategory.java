package com.hoang.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hoang.domain.Category;

@Transactional
public interface DAOCategory extends JpaRepository<Category, Integer>{

	Category findByCategoryName(String categoryName);
	
	@Query("SELECT c FROM Category c where c.categoryName like %:search% OR c.description like %:search%")
	List<Category> findAllByCategoryNameOrDescription(@Param("search") String search);
}
