package com.hoang.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.DAO.DAOJob;
import com.hoang.DAO.DAOUser;
import com.hoang.domain.User;
import com.hoang.service.ServiceUser;


@Controller
public class ControllerUpload {

	@Autowired
	DAOUser service;
	
	@Autowired
	DAOJob job;
	
	@Autowired
	ServiceUser user;
	
    //Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "E:\\";
    private static String FILE_NAME = "";

    @GetMapping("/trainer")
    public String index() {
        return "upload";
    }

    @SuppressWarnings("deprecation")
	@PostMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);
            FILE_NAME = UPLOADED_FOLDER + file.getOriginalFilename();
            
            try {

                FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
                @SuppressWarnings("resource")
				Workbook workbook = new XSSFWorkbook(excelFile);
                Sheet datatypeSheet = workbook.getSheetAt(0);
                Iterator<Row> iterator = datatypeSheet.iterator();
                
                

                while (iterator.hasNext()) {
                	int count = 0;
                    Row currentRow = iterator.next();
                    Iterator<Cell> cellIterator = currentRow.iterator();
                    User temp = new User();
                    
                    while (cellIterator.hasNext()) {
                    	
                        Cell currentCell = cellIterator.next();
                        
                        if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        	System.out.println("x");
                            System.out.println(currentCell.getStringCellValue() + "--");
                        } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        	System.out.println("x");
                            System.out.println(currentCell.getNumericCellValue() + "--");
                        }
                        
                        count++;
                        
                        
                        if(count == 1)
                        {
                        	if (currentCell.getCellTypeEnum() == CellType.STRING)
                        	{
                                temp.setAccount(currentCell.getStringCellValue());
                        	}
                        	else if(currentCell.getCellTypeEnum() == CellType.NUMERIC){
                        		String x = (int)currentCell.getNumericCellValue()+"";
                        		temp.setAccount(x);
                        	}
                        }                     
                        else if(count == 2){
                        		if (currentCell.getCellTypeEnum() == CellType.STRING) {
                                    temp.setPassword(currentCell.getStringCellValue());
                                } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                                	int a = (int)currentCell.getNumericCellValue();
                                	temp.setPassword(a + "");
                                }
                        		System.out.println(temp.getPassword());
                        	}
                        	temp.setJob(job.findByJobName("trainee"));

                    }
                    System.out.println();
                    user.them(temp);
                }
                
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/uploadStatus";
    }

    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }

}
