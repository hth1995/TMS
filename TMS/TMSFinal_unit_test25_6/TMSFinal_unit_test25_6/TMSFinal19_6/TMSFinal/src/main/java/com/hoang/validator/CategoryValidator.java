package com.hoang.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hoang.domain.Category;
import com.hoang.service.ServiceCategory;



@Component
public class CategoryValidator implements Validator {
	 @Autowired
	 private ServiceCategory serviceCategory;
	 
	
	
	  @Override
	    public boolean supports(Class<?> aClass) {
	        return Category.class.equals(aClass);
	    }
	  

	  @Override
	    public void validate(Object o, Errors errors) {
		  @SuppressWarnings("unused")
		Category category = (Category) o;

	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryName", "NotEmpty.category.categoryName");
	        
	      
	    }
}
