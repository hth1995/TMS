package com.hoang.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hoang.domain.Property;
import com.hoang.domain.Topic;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;
import com.hoang.domain.trainer_info;
import com.hoang.service.ServiceCourse;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceProperty;
import com.hoang.service.ServiceTopic;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;



@Controller
@RequestMapping(value="/formtrainer")
public class ControllerTrainer {

	@Autowired
	ServiceCourse course;
	@Autowired
	ServiceUser user;
	@Autowired
	ServiceTopic topic;
	@Autowired
	ServiceJob job;
	@Autowired
	UserDetailsService detail;
	@Autowired
	ServiceProperty property;
	@Autowired
	ServiceUserProperty userpro;
	
	@RequestMapping(value="/viewcourse")
	public String xem(Model model){
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username;
		if (principal instanceof UserDetails) {
		    username = ((UserDetails) principal).getUsername();
		} else {
		    username = principal.toString();
		}
		User a=user.findByAccount(username);
		List <Topic> l= new ArrayList<>();
		l = topic.findAllByUsername(a);
		model.addAttribute("topic", new Topic());
		model.addAttribute("listTopic", l);
		model.addAttribute("listCourse",course.lietKe());
		return "viewcourse";
	}
	
	@RequestMapping(value="/xulyxem")
	public String xem(@ModelAttribute Topic temp,Model model){
		Topic b=topic.findById(temp.getTopicId());
		model.addAttribute("listCourse",course.findbytopic(b));
		return "viewcourse";
	}
	
	@RequestMapping(value="/trainer_update_info")
	public String update(Model model){
	//	model.addAttribute("trainer_info", new trainer_info());
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username;
		if (principal instanceof UserDetails) {
		    username = ((UserDetails) principal).getUsername();
		} else {
		    username = principal.toString();
		}
		User a=user.findByAccount(username);
		List <Userproperty> l = new ArrayList<Userproperty>();
		l=userpro.findAllByUser(a);
		if(!l.isEmpty())
		{
			trainer_info temp = new trainer_info();
			for( int i = 0 ; i < l.size(); i++)
			{
			if(l.get(i).getProperty().getPropertyName().equals("name"))
				temp.setName(l.get(i).getPropertyValue());
			if(l.get(i).getProperty().getPropertyName().equals("type"))			
				temp.setType(l.get(i).getPropertyValue());
			if(l.get(i).getProperty().getPropertyName().equals("education"))
				temp.setEducation(l.get(i).getPropertyValue());
			if(l.get(i).getProperty().getPropertyName().equals("working_place"))
				temp.setPlace(l.get(i).getPropertyValue());
			if(l.get(i).getProperty().getPropertyName().equals("telephone"))
				temp.setTelephone(l.get(i).getPropertyValue());
			if(l.get(i).getProperty().getPropertyName().equals("email"))
				temp.setEmail(l.get(i).getPropertyValue());				
			}
			model.addAttribute("info", temp);
		}
		else
		{
			model.addAttribute("info", new trainer_info());
		}
		
		List <Userproperty> l1 = new ArrayList<Userproperty>();
		l1=userpro.findAllByUser(a);
		List <String> temp1=new ArrayList<>();
		System.out.println(l1.size());
		for(int i=0;i<l1.size();i++)
		{
			temp1.add(l1.get(i).getPropertyValue());
		}
		model.addAttribute("listinfo", temp1);
		return "trainer_update_info";
	}
	
	@RequestMapping(value="/xulysua")
	public String xylyupdate(@ModelAttribute trainer_info temp,Model model){
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username;
		if (principal instanceof UserDetails) {
		    username = ((UserDetails) principal).getUsername();
		} else {
		    username = principal.toString();
		}
		User a=user.findByAccount(username);
		UserpropertyId usproid = new UserpropertyId();
		usproid.setUserId(a.getUserId());
		Userproperty usertemp = new Userproperty();
		usertemp.setUser(a);
		usertemp.setPropertyValue(temp.getName());
		Property pro = property.findByPropertyName("name");
		usproid.setPropertyId(pro.getPropertyId());
		usertemp.setProperty(pro);
		usertemp.setId(usproid);
		userpro.them(usertemp);
		
		usertemp.setPropertyValue(temp.getEducation());
		pro = property.findByPropertyName("education");
		usproid.setPropertyId(pro.getPropertyId());
		usertemp.setProperty(pro);
		usertemp.setId(usproid);
		userpro.them(usertemp);
		
		usertemp.setPropertyValue(temp.getType());
		pro = property.findByPropertyName("type");
		usproid.setPropertyId(pro.getPropertyId());
		usertemp.setProperty(pro);
		usertemp.setId(usproid);
		userpro.them(usertemp);
		

		usertemp.setPropertyValue(temp.getPlace());
		pro = property.findByPropertyName("working_place");
		usproid.setPropertyId(pro.getPropertyId());
		usertemp.setProperty(pro);
		usertemp.setId(usproid);
		userpro.them(usertemp);
		

		usertemp.setPropertyValue(temp.getTelephone());
		pro = property.findByPropertyName("telephone");
		usproid.setPropertyId(pro.getPropertyId());
		usertemp.setProperty(pro);
		usertemp.setId(usproid);
		userpro.them(usertemp);
	
		usertemp.setPropertyValue(temp.getEmail());
		pro = property.findByPropertyName("email");
		usproid.setPropertyId(pro.getPropertyId());
		usertemp.setProperty(pro);
		usertemp.setId(usproid);
		userpro.them(usertemp);
		return "redirect:/formtrainer/trainer_update_info";
	}

	@RequestMapping(value="/xoainfo")
	public String xoa(){
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username;
		if (principal instanceof UserDetails) {
		    username = ((UserDetails) principal).getUsername();
		} else {
		    username = principal.toString();
		}
		User a= user.findByAccount(username);
		List <Userproperty> l = new ArrayList<Userproperty>();
		l=userpro.findAllByUser(a);
		for(int i=0;i<l.size();i++)
		{
			Userproperty temp = l.get(i);
			userpro.delete(temp);
		}
		return "redirect:/formtrainer/trainer_update_info";
	}
	
}
