package com.hoang.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hoang.domain.Property;
import com.hoang.service.ServiceProperty;


@Component
public class PropertyValidator implements Validator {

	@Autowired
    private ServiceProperty serviceProperty;

    @Override
    public boolean supports(Class<?> aClass) {
        return Property.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
    	@SuppressWarnings("unused")
		Property user = (Property) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propertyName", "NotEmpty.property.propertyName");
      
      
    }
}
