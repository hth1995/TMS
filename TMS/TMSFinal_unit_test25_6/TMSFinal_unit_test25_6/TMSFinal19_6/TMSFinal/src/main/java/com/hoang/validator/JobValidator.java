package com.hoang.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hoang.domain.Job;
import com.hoang.service.ServiceJob;

@Component
public class JobValidator implements Validator{
	
	@Autowired
    private ServiceJob serviceJob;

    @Override
    public boolean supports(Class<?> aClass) {
        return Job.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
    	@SuppressWarnings("unused")
		Job job = (Job) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "jobName", "NotEmpty.job.jobName");
       
      
    }

	
}
