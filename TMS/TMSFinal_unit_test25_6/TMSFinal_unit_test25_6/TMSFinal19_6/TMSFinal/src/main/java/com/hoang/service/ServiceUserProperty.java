package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOUserProperty;
import com.hoang.domain.Job;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;

@Service
public class ServiceUserProperty {
	@Autowired
	DAOUserProperty service;
	
	public void them(Userproperty up){
		service.save(up);
	}
	
	public void delete (Userproperty id)
	{
		service.delete(id);
	}
	
	public void xoa(int idPropertyId,int idUserId){
		service.delete(service.findAllByIdPropertyIdAndIdUserId(idPropertyId, idUserId));
	}
	
	public Userproperty findOneSV(UserpropertyId id){
		return service.findOne(id);
	}
	
	public Userproperty findAllByIdPropertyIdAndIdUserId(int idPropertyId,int idUserId){
		return service.findAllByIdPropertyIdAndIdUserId(idPropertyId, idUserId);
	}
	
	public List<Userproperty> findAllByUserJob(Job job){
		return service.findAllByUserJob(job);
	}
	
	
	public void sua(Userproperty tp){
		Userproperty temp = service.findOne(tp.getId());
		temp.setPropertyValue(tp.getPropertyValue());
		service.save(temp);
	}
	
	public List<Userproperty> findAllByUserAccount(String account){
		return service.findAllByUserAccount(account);
	}

	public List<Userproperty> lietKe(){
		return service.findAll();
	}
	
	public List<Userproperty> findAllByPropertyPropertyNameAndPropertyValue(String colum,String search){
		return service.findAllByPropertyPropertyNameAndPropertyValue(colum, search);
		
	}
	
	public List<Userproperty> findAllByUser(User a)
	{
		return service.findAllByUser(a);
	}
	public void deleteAllByUserAccount(String account){
		List<Userproperty> a = service.findAllByUserAccount(account);
		for(int i = 0 ; i < a.size() ; i++)
		{
			service.delete(service.findAllByIdPropertyIdAndIdUserId(a.get(i).getId().getPropertyId(), a.get(i).getId().getUserId()));
		}
	}
	
} 
