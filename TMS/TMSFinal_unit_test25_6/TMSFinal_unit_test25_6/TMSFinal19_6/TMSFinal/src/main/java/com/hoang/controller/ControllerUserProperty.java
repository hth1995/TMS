package com.hoang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hoang.domain.Property;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;
import com.hoang.service.ServiceProperty;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;

@Controller
@RequestMapping(value="/formuserproperty")
public class ControllerUserProperty {
	@Autowired
	ServiceUserProperty userProperty;
	@Autowired
	ServiceUser user;
	@Autowired
	ServiceProperty property;
	
	@RequestMapping(value="/themuserproperty")
	public String them(Model model){
		
		model.addAttribute("userPropertyId",new UserpropertyId());
		model.addAttribute("userProperty",new Userproperty());
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		model.addAttribute("listUserProperty",userProperty.lietKe());
		model.addAttribute("listProperty",property.lietKe());
		model.addAttribute("listUser",user.lietKe());
//		session = request.getSession();
//		model.addAttribute("loggedInUser", session);
		return "themuserproperty";
	}
	
	@RequestMapping(value="/xulythem")
	public String xulythem(@ModelAttribute Userproperty u){
		userProperty.them(u);
		return "redirect:/formuserproperty/themuserproperty";
	}
	
	@RequestMapping(value="/xoauserproperty/{id1}/{id2}")
	public String xoa(@PathVariable String id1,@PathVariable String id2){
		userProperty.xoa(Integer.parseInt(id1),Integer.parseInt(id2));
		return "redirect:/formuserproperty/themuserproperty";
	}
	
	@RequestMapping(value="/suauserproperty/{id1}/{id2}")
	public String sua(@PathVariable String id1,@PathVariable String id2,Model model){
		model.addAttribute("userProperty",userProperty.findAllByIdPropertyIdAndIdUserId(Integer.parseInt(id1), Integer.parseInt(id2)));
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		model.addAttribute("listUserProperty",userProperty.lietKe());
		model.addAttribute("listProperty",property.lietKe());
		model.addAttribute("listUser",user.lietKe());
		return "suauserproperty";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Userproperty u){
		userProperty.sua(u);
		return "redirect:/formuserproperty/themuserproperty";
	}
	
	@RequestMapping("/xulyxoa/{id}")
	public String delete(@ModelAttribute Userproperty id) {
		userProperty.delete(id);
	    return "/formtraining_info/listTrainingInfoTrainee";
	 }
	
}
