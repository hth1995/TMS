package com.hoang.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;



import com.hoang.domain.Topic;
import com.hoang.service.ServiceTopic;

@Component
public class TopicValidator implements Validator{

	
	 @Autowired
	 private ServiceTopic serviceTopic;
	 
	 @Override
	    public boolean supports(Class<?> aClass) {
	        return Topic.class.equals(aClass);
	    }
	  

	  @Override
	    public void validate(Object o, Errors errors) {
		  @SuppressWarnings("unused")
		Topic topic = (Topic) o;

	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "topicName", "NotEmpty.topic.topicName");
	        
	      
	    }
	 
}
