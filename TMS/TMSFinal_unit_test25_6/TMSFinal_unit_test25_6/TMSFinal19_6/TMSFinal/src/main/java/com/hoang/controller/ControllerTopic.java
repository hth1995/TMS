package com.hoang.controller;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.config.SmtpMailSender;
import com.hoang.domain.Course;
import com.hoang.domain.Job;
import com.hoang.domain.Property;
import com.hoang.domain.Topic;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.service.ServiceCourse;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceProperty;
import com.hoang.service.ServiceTopic;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;
import com.hoang.validator.TopicValidator;

@Controller
@RequestMapping(value="/formtopic")
public class ControllerTopic {
	
	@Autowired
	ServiceProperty property;
	
	@Autowired
	private SmtpMailSender smtpMailSender;
	
	@Autowired
	private JavaMailSender javaMailSender;	
	
	@Autowired
	ServiceUserProperty userproperty;
	
	@Autowired
	ServiceCourse course;
	@Autowired
	ServiceUser user;
	@Autowired
	ServiceTopic topic;
	
	@Autowired
	ServiceJob job;
	
	@Autowired
	TopicValidator topicValidator;
	
	 @InitBinder
	   protected void initBinder(WebDataBinder dataBinder) {
	   
	       // Form mục tiêu
	       Object target = dataBinder.getTarget();
	       if (target == null) {
	           return;
	       }
	       System.out.println("Target=" + target);
	 
	       if (target.getClass() == Topic.class) {
	           dataBinder.setValidator(topicValidator);
	       }
	   }
	
	@RequestMapping(value="/themtopic")
	public String them(Model model){
		model.addAttribute("topic", new Topic());
		model.addAttribute("course",new Course());
		model.addAttribute("user",new User());
		model.addAttribute("listTopic",topic.lietKe());
		model.addAttribute("listCourse",course.lietKe());
		List<User> l = new ArrayList<User>();
		Job temp = job.findByJobName("trainer");
		l = user.findAllByJob(temp);
		model.addAttribute("listUser",l);
		return "themtopic";
	}
	

	

	@RequestMapping(value="/xulythem",method=RequestMethod.POST)
	public String xulythem(Model model,@ModelAttribute("topic") @Validated Topic tp, BindingResult result, final RedirectAttributes redirectAttributes) throws MessagingException{
		//userValidator.validate(us, result);
		
		 if(result.hasErrors()){
			 return "themtopic";	
		 }
		 
		 topic.them(tp); 
			Userproperty temp = new Userproperty();
			Property a = property.findByPropertyName("email");
			temp = userproperty.findAllByIdPropertyIdAndIdUserId(a.getPropertyId(), tp.getUser().getUserId());
			
			if(!temp.getPropertyValue().equals("")){
				SmtpMailSender mail = new SmtpMailSender(javaMailSender);		
				mail.send(temp.getPropertyValue(), "Thông báo add topic", "Topic: " + tp.getTopicName() + "\n Mã topic: " + tp.getTopicId() + "\n Description:"+tp.getDescription()+"\n đã được đăng ký cho tài khoản của bạn!");
			}
		 return "redirect:/formtopic/themtopic";
		
	}
	@RequestMapping(value="/xoatopic/{id}")
	public String xoa(@PathVariable String id){
		topic.xoa(Integer.parseInt(id));
		return "redirect:/formtopic/themtopic";
	}
	
	@RequestMapping(value="/suatopic/{id}")
	public String sua(@PathVariable String id,Model model){
		model.addAttribute("topic",topic.findOneSV(Integer.parseInt(id)));
		List<User> l = new ArrayList<User>();
		Job temp = job.findByJobName("trainer");
		l = user.findAllByJob(temp);
		model.addAttribute("listUser",l);
		model.addAttribute("listCourse",course.lietKe());
		model.addAttribute("listTopic",topic.lietKe());
		return "suatopic";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Topic tp){
		topic.sua(tp);
		return "redirect:/formtopic/themtopic";
	}

	@RequestMapping(value="/xulysearch")
	public String xulysearch(@RequestParam("search") String search,Model model){
		
		if(search.equals(""))
		{
			return "redirect:/formtopic/themtopic";
		}
		model.addAttribute("topic", new Topic());
		model.addAttribute("course",new Course());
		model.addAttribute("user",new User());
		model.addAttribute("listTopic",topic.findAllByTopicNameOrDescription(search));
		model.addAttribute("listCourse",course.lietKe());
		List<User> l = new ArrayList<User>();
		Job temp = job.findByJobName("trainer");
		l = user.findAllByJob(temp);
		model.addAttribute("listUser",l);
	
		
		return "themtopic";
	}

}
