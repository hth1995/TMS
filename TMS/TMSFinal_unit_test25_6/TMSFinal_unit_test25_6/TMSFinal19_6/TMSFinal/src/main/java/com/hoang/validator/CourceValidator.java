package com.hoang.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;



import com.hoang.domain.Course;
import com.hoang.service.ServiceCourse;

@Component
public class CourceValidator implements Validator{
	 @Autowired
	 private ServiceCourse serviceCourse;
	 
	 @Override
	    public boolean supports(Class<?> aClass) {
	        return Course.class.equals(aClass);
	    }
	 

	  @Override
	    public void validate(Object o, Errors errors) {
		  @SuppressWarnings("unused")
		Course course = (Course) o;

	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseName", "NotEmpty.course.courseName");
	        
	      
	    }
	
}
