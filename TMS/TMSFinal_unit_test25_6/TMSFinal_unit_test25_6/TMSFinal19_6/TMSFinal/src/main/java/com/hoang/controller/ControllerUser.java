package com.hoang.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.domain.Job;
import com.hoang.domain.User;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceUser;
import com.hoang.validator.UserValidator;

@Controller
@RequestMapping(value="/formuser")
public class ControllerUser {

	@Autowired
	ServiceJob job;
	@Autowired
	ServiceUser user;
	
	@Autowired 
    private PasswordEncoder passwordEncoder;
	
	
	@Autowired
	UserValidator userValidator;
	
	 @InitBinder
	   protected void initBinder(WebDataBinder dataBinder) {
	   
	       // Form mục tiêu
	       Object target = dataBinder.getTarget();
	       if (target == null) {
	           return;
	       }
	       System.out.println("Target=" + target);
	 
	       if (target.getClass() == User.class) {
	           dataBinder.setValidator(userValidator);
	       }
	   }
	@RequestMapping(value="/themuser")
	public String them(Model model){
		
		model.addAttribute("user",new User());
		model.addAttribute("job",new Job());
		model.addAttribute("listJob",job.lietKe());
		model.addAttribute("listUser",user.lietKe());
//		session = request.getSession();
//		model.addAttribute("loggedInUser", session);
		return "themuser";
	}
	
//	@RequestMapping(value="/xulythem")
//	public String xulythem(@ModelAttribute User u){
//		user.them(u);
//		return "redirect:/formuser/themuser";
//	}
	
	@RequestMapping(value="/xulythem",method=RequestMethod.POST)
	public String xulythem(Model model,@ModelAttribute("user") @Validated User us, BindingResult result, final RedirectAttributes redirectAttributes){
		//userValidator.validate(us, result);
		 if(result.hasErrors()){
			 return "themuser";	
		 }
		 user.them(us); 
		 return "redirect:/formuser/themuser";
		
	}
	@RequestMapping(value="/xoauser/{id}")
	public String xoa(@PathVariable String id){
		user.xoa(Integer.parseInt(id));
		return "redirect:/formuser/themuser";
	}
	
	@RequestMapping(value="/suauser/{id}")
	public String sua(@PathVariable String id,Model model){
		
		model.addAttribute("user",user.findOneSV(Integer.parseInt(id)));
		model.addAttribute("listJob",job.lietKe());
		model.addAttribute("listUser",user.lietKe());
		return "suauser";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute User u){
		user.sua(u);
		return "redirect:/formuser/themuser";
	}
	
	@RequestMapping(value="/xulylogout")
	public String logout(HttpSession session){
		session.removeAttribute("loggedInUser");
		return "redirect:/formlogin/login";
	}
}
