package com.hoang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.domain.Category;
import com.hoang.domain.User;
import com.hoang.service.ServiceCategory;
import com.hoang.service.ServiceCourse;
import com.hoang.validator.CategoryValidator;


@Controller
@RequestMapping(value="/form")
public class ControllerCategory {
	
	@Autowired
	ServiceCategory category;
	@Autowired
	ServiceCourse course;
	@Autowired
	CategoryValidator categoryValidator;
	@InitBinder
	   protected void initBinder(WebDataBinder dataBinder) {
	   
	       // Form mục tiêu
	       Object target = dataBinder.getTarget();
	       if (target == null) {
	           return;
	       }
	       System.out.println("Target=" + target);
	 
	       if (target.getClass() == User.class) {
	           dataBinder.setValidator(categoryValidator);
	       }
	   }
	
	@RequestMapping(value="/them")
	public String them(Model model){
		model.addAttribute("category",new Category());
		model.addAttribute("listCategory",category.lietKe());
		return "them";
	}
	
//	@RequestMapping(value="/xulythem")
//	public String xulythem(@ModelAttribute Category ct){
//		category.them(ct);
//		return "redirect:/form/them";
//	}
	@RequestMapping(value="/xulythem",method=RequestMethod.POST)
	public String xulythem(Model model,@ModelAttribute("category") @Validated Category ct, BindingResult result, final RedirectAttributes redirectAttributes){
		//userValidator.validate(us, result);
		 if(result.hasErrors()){
			 return "them";	
		 }
		 category.them(ct); 
		 return "redirect:/form/them";
		
	}
	
//	@RequestMapping(value="/xoa/{id}")
//	public String xoa(@PathVariable String id){
//		category.xoa(Integer.parseInt(id));
//		return "redirect:/form/them";
//	}
	@RequestMapping(value="/xoa/{id}")
	public String xoa(@PathVariable String id){
		try
		{
			category.xoa(Integer.parseInt(id));
		}
		catch (Exception e)
		{
			return "403";
		}
		return "redirect:/form/them";
	}
	
	@RequestMapping(value="/sua/{id}")
	public String sua(@PathVariable String id,Model model){
		model.addAttribute("category",category.findOneSV(Integer.parseInt(id)));
		model.addAttribute("listCategory",category.lietKe());
		return "sua";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Category ct){
		category.sua(ct);
		return "redirect:/form/them";
	}
	
	@RequestMapping(value="/xulysearch")
	public String xulysearch(@RequestParam("search") String search,Model model){
		
		if(search.equals(""))
		{
			return "redirect:/form/them";
		}
		model.addAttribute("category",new Category());
		model.addAttribute("listCategory",category.findAllByCategoryNameOrDescription(search));
		return "them";
	}

}
