package com.hoang.domain;

public class trainer_info {
	 private User user;
	 
public String name,place,telephone,email,type,education;


public trainer_info(User user) {
	super();
	this.user = user;
}

public User getUser() {
	return user;
}

public void setUser(User user) {
	this.user = user;
}

public String getEducation() {
	return education;
}

public void setEducation(String education) {
	this.education = education;
}

public trainer_info(String name, String place, String telephone, String email, String type) {
	super();
	this.name = name;
	this.place = place;
	this.telephone = telephone;
	this.email = email;
	this.type = type;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getPlace() {
	return place;
}

public trainer_info() {
	super();
}

public void setPlace(String place) {
	this.place = place;
}

public String getTelephone() {
	return telephone;
}

public void setTelephone(String telephone) {
	this.telephone = telephone;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}
}
