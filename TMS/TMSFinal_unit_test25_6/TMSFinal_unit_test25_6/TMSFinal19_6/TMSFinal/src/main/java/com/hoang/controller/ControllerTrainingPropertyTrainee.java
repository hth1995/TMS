package com.hoang.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import com.hoang.domain.ClassTraineeProfile;

import com.hoang.domain.Property;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceProperty;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;

@Controller
@RequestMapping(value="/formtrainingpropertytrainee")
public class ControllerTrainingPropertyTrainee {
	@Autowired
	ServiceUserProperty userProperty;
	@Autowired
	ServiceUser user;
	@Autowired
	ServiceProperty property;
	@Autowired
	ServiceJob job;
	
	@RequestMapping(value="/training_addproperty_trainee")
	public String them(Model model){
		
//		model.addAttribute("userPropertyId",new UserpropertyId());
//		model.addAttribute("userProperty",new Userproperty());
//		model.addAttribute("user",new User());
//		model.addAttribute("property",new Property());
//		model.addAttribute("listUserProperty",userProperty.findAllByUserJob(job.findByJobName("trainer")));
//		List<Property> l1 = new ArrayList<Property>();
//		l1.add(property.findByPropertyName("name"));
//		l1.add(property.findByPropertyName("external"));
//		l1.add(property.findByPropertyName("internal"));
//		l1.add(property.findByPropertyName("working_place"));
//		l1.add(property.findByPropertyName("telephone"));
//		l1.add(property.findByPropertyName("email"));
//		model.addAttribute("listProperty",l1);
//		List<User> l = new ArrayList<User>();
//		l = user.findAllByJob(job.findByJobName("trainer"));
//		model.addAttribute("listUser",l);
//		return "training_addproperty_trainee";
		
		model.addAttribute("userPropertyId",new UserpropertyId());
		model.addAttribute("userProperty",new Userproperty());
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		
		List<Userproperty> up = new ArrayList<Userproperty>();
		up = userProperty.findAllByUserJob(job.findByJobName("trainee"));
		List<ClassTraineeProfile> x = new ArrayList<>();
		if(!up.isEmpty()) // khac null moi show ra
		for(int i = 0; i < up.size() ; i++)
		{
			if(x.isEmpty()) // truong hop dau tien rong thi tao moi
			{
				ClassTraineeProfile a = new ClassTraineeProfile();
				a.setAccount(up.get(i).getUser().getAccount());
				a.setUserid(user.findByAccount(a.getAccount()).getUserId());
				x.add(a);
			}
			for(int j = 0 ; j < x.size() ;j++)
			{
				if(up.get(i).getUser().getAccount().equals(x.get(j).getAccount()))
				{
					if(up.get(i).getProperty().getPropertyName().equals("name"))
						{
							x.get(j).setName(up.get(i).getPropertyValue());
							System.out.println(x.get(j).getAccount()+"  Name: "+up.get(i).getPropertyValue());
						}
					if(up.get(i).getProperty().getPropertyName().equals("age"))
						x.get(j).setAge(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("date_of_birth"))
						x.get(j).setDate_of_birth(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("education"))
						x.get(j).setEducation(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("main_program_language"))
						x.get(j).setMain_program_language(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("toeic_score"))
						x.get(j).setToeic_score(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("experience"))
						x.get(j).setExperience(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("department"))
						x.get(j).setDepartment(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("location"))
						x.get(j).setLocation(up.get(i).getPropertyValue());
					break;
				}
				else
				{ // khong tim duoc thi tao moi va them vo
					if(!up.get(i).getUser().getAccount().equals(x.get(j).getAccount()) && j == x.size()-1)
					{
						ClassTraineeProfile a = new ClassTraineeProfile();
						a.setAccount(up.get(i).getUser().getAccount());
						a.setUserid(user.findByAccount(a.getAccount()).getUserId());
						
						if(up.get(i).getProperty().getPropertyName().equals("name"))
							a.setName(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("age"))
							a.setAge(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("date_of_birth"))
							a.setDate_of_birth(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("education"))
							a.setEducation(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("main_program_language"))
							a.setMain_program_language(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("toeic_score"))
							a.setToeic_score(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("experience"))
							a.setExperience(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("department"))
							a.setDepartment(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("location"))
							a.setLocation(up.get(i).getPropertyValue());				
						x.add(a);
						break;
					}
				}
			}				
		}
		model.addAttribute("listUserProperty",x);
		model.addAttribute("name", new String());
		model.addAttribute("age", new String());
		model.addAttribute("date_of_birth", new String());
		model.addAttribute("education", new String());
		model.addAttribute("main_program_language", new String());
		model.addAttribute("toeic_score", new String());
		model.addAttribute("experience", new String());
		model.addAttribute("department", new String());
		model.addAttribute("location", new String());
		
		List<User> l = new ArrayList<User>();
		l = user.findAllByJob(job.findByJobName("trainee"));
		
		// chi xuat ra nhung tai khoan chua co property
				// x la list nhung tai khoan co property
				for(int i = 0 ; i < x.size() ; i++)
				{
					for(int j = 0 ; j < l.size() ; j++)
					{
						if(l.get(j).getAccount().equals(x.get(i).getAccount()))
						{
							l.remove(j);
							break;
						}
					}
				}
				model.addAttribute("listUser",l);	
				List<Property> l2 = new ArrayList<>();
				l2.add(property.findByPropertyName("name"));
				l2.add(property.findByPropertyName("age"));
				l2.add(property.findByPropertyName("date_of_birth"));
				l2.add(property.findByPropertyName("education"));
				l2.add(property.findByPropertyName("main_program_language"));
				l2.add(property.findByPropertyName("toeic_score"));
				l2.add(property.findByPropertyName("experience"));
				l2.add(property.findByPropertyName("department"));
				l2.add(property.findByPropertyName("location"));
				model.addAttribute("listProperty",l2);		
				
		return "training_addproperty_trainee";
	}
	
//	@RequestMapping(value="/xulythem")
//	public String xulythem(@ModelAttribute Userproperty u){
//		userProperty.them(u);
//		return "redirect:/training_addproperty_trainee/training_addproperty_trainee";
//	}
//	
	@RequestMapping(value="/xulythem")
	public String xulythem(@ModelAttribute Userproperty u,@RequestParam("name") String name,@RequestParam("age") String age,
			@RequestParam("date_of_birth") String date_of_birth,@RequestParam("education") String education,
			@RequestParam("main_program_language") String main_program_language,@RequestParam("toeic_score") String toeic_score,@RequestParam("experience") String experience,
			@RequestParam("department") String department,@RequestParam("location") String location
			)
	{
		int userid = u.getId().getUserId();
		Property temp = new Property();
		temp = property.findByPropertyName("name");
		u.getId().setPropertyId(temp.getPropertyId());
		u.getId().setUserId(userid);
		
		List<Userproperty> up = new ArrayList<Userproperty>();
		up = userProperty.findAllByUserAccount(user.findOneSV(u.getId().getUserId()).getAccount());
		// kiem tra xem account co profile chua chua co thi tao moi
		if(up.isEmpty())
		{
			Integer iduser = u.getId().getUserId();
			
			UserpropertyId upid = new UserpropertyId();
			Userproperty up1 = new Userproperty();
			
			Property puser = property.findByPropertyName("name");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);	
			up1.setId(upid);
			up1.setPropertyValue(name);
			userProperty.them(up1);
			
			puser = property.findByPropertyName("age");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);		
			up1.setId(upid);
			up1.setPropertyValue(age);
			userProperty.them(up1);
			
			puser = property.findByPropertyName("date_of_birth");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);		
			up1.setId(upid);
			up1.setPropertyValue(date_of_birth);
			userProperty.them(up1);
			
			puser = property.findByPropertyName("education");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);		
			up1.setId(upid);
			up1.setPropertyValue(education);
			userProperty.them(up1);
			
			puser = property.findByPropertyName("main_program_language");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);		
			up1.setId(upid);
			up1.setPropertyValue(main_program_language);
			userProperty.them(up1);
			
			puser = property.findByPropertyName("toeic_score");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);		
			up1.setId(upid);
			up1.setPropertyValue(toeic_score);
			userProperty.them(up1);
			
			puser = property.findByPropertyName("experience");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);		
			up1.setId(upid);
			up1.setPropertyValue(toeic_score);
			userProperty.them(up1);
			
			puser = property.findByPropertyName("department");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);		
			up1.setId(upid);
			up1.setPropertyValue(department);
			userProperty.them(up1);
			
			puser = property.findByPropertyName("location");
			upid.setPropertyId(puser.getPropertyId());
			upid.setUserId(iduser);		
			up1.setId(upid);
			up1.setPropertyValue(location);
			userProperty.them(up1);
		}
		else
		{
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(name))
			{
				u.setPropertyValue(name);
				userProperty.sua(u);
			}
			
			temp = property.findByPropertyName("age");
			u.getId().setPropertyId(temp.getPropertyId());
			u.getId().setUserId(userid);
			
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(age))
			{
				u.setPropertyValue(age);
				userProperty.sua(u);
			}
			
			temp = property.findByPropertyName("date_of_birth");
			u.getId().setPropertyId(temp.getPropertyId());
			u.getId().setUserId(userid);
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(date_of_birth))
			{
				u.setPropertyValue(date_of_birth);
				userProperty.sua(u);
			}
			temp = property.findByPropertyName("education");
			u.getId().setPropertyId(temp.getPropertyId());
			u.getId().setUserId(userid);
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(education))
			{	
				u.setPropertyValue(education);
				userProperty.sua(u);
			}
			
			temp = property.findByPropertyName("main_program_language");
			u.getId().setPropertyId(temp.getPropertyId());
			u.getId().setUserId(userid);
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(main_program_language))
			{
				u.setPropertyValue(main_program_language);
				userProperty.sua(u);
			}
			
			temp = property.findByPropertyName("toeic_score");
			u.getId().setPropertyId(temp.getPropertyId());
			u.getId().setUserId(userid);
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(toeic_score))
			{
				u.setPropertyValue(toeic_score);
				userProperty.sua(u);
			}
			
			temp = property.findByPropertyName("experience");
			u.getId().setPropertyId(temp.getPropertyId());
			u.getId().setUserId(userid);
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(experience))
			{
				u.setPropertyValue(experience);
				userProperty.sua(u);
			}
			
			temp = property.findByPropertyName("department");
			u.getId().setPropertyId(temp.getPropertyId());
			u.getId().setUserId(userid);
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(department))
			{
				u.setPropertyValue(department);
				userProperty.sua(u);
			}
			
			temp = property.findByPropertyName("location");
			u.getId().setPropertyId(temp.getPropertyId());
			u.getId().setUserId(userid);
			if(!userProperty.findAllByIdPropertyIdAndIdUserId(temp.getPropertyId(), u.getId().getUserId()).getPropertyValue().equals(location))
			{
				u.setPropertyValue(location);
				userProperty.sua(u);
			}
		}
		
		
		return "redirect:/formtrainingpropertytrainee/training_addproperty_trainee";
	}
	
	@RequestMapping(value="/xoauserproperty/{id}")
	public String xoa(@PathVariable String id){
		userProperty.deleteAllByUserAccount(user.findOneSV(Integer.parseInt(id)).getAccount());
		return "redirect:/formtrainingpropertytrainee/training_addproperty_trainee";
	}
	
	@RequestMapping(value="/training_editproperty_trainee/{id}")
	public String sua(@PathVariable String id,Model model){

		model.addAttribute("userPropertyId",new UserpropertyId());
		model.addAttribute("userProperty",new Userproperty());
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		List<User> l = new ArrayList<User>();
		l.add(user.findOneSV(Integer.parseInt(id)));
		
		List<Userproperty> up = new ArrayList<Userproperty>();
		up = userProperty.findAllByUserAccount(user.findOneSV(Integer.parseInt(id)).getAccount());
		List<ClassTraineeProfile> x = new ArrayList<>();
		if(!up.isEmpty()) // khac null moi show ra
			for(int i = 0; i < up.size() ; i++)
			{
				if(x.isEmpty()) // truong hop dau tien rong thi tao moi
				{
					ClassTraineeProfile a = new ClassTraineeProfile();
					a.setAccount(up.get(i).getUser().getAccount());
					a.setUserid(user.findByAccount(a.getAccount()).getUserId());
					x.add(a);
				}
				for(int j = 0 ; j < x.size() ;j++)
				{
					if(up.get(i).getUser().getAccount().equals(x.get(j).getAccount()))
					{
						if(up.get(i).getProperty().getPropertyName().equals("name"))
							x.get(j).setName(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("age"))
							x.get(j).setAge(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("date_of_birth"))
							x.get(j).setDate_of_birth(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("education"))
							x.get(j).setEducation(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("main_program_language"))
							x.get(j).setMain_program_language(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("toeic_score"))
							x.get(j).setToeic_score(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("experience"))
							x.get(j).setExperience(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("department"))
							x.get(j).setDepartment(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("location"))
							x.get(j).setLocation(up.get(i).getPropertyValue());
						break;
					}
					else
					{ // khong tim duoc thi tao moi va them vo
						if(!up.get(i).getUser().getAccount().equals(x.get(j).getAccount()) && j == x.size()-1)
						{
							ClassTraineeProfile a = new ClassTraineeProfile();
							a.setAccount(up.get(i).getUser().getAccount());
							a.setUserid(user.findByAccount(a.getAccount()).getUserId());
							
							if(up.get(i).getProperty().getPropertyName().equals("name"))
								x.get(j).setName(up.get(i).getPropertyValue());
							if(up.get(i).getProperty().getPropertyName().equals("age"))
								x.get(j).setAge(up.get(i).getPropertyValue());
							if(up.get(i).getProperty().getPropertyName().equals("date_of_birth"))
								x.get(j).setDate_of_birth(up.get(i).getPropertyValue());
							if(up.get(i).getProperty().getPropertyName().equals("education"))
								x.get(j).setEducation(up.get(i).getPropertyValue());
							if(up.get(i).getProperty().getPropertyName().equals("main_program_language"))
								x.get(j).setMain_program_language(up.get(i).getPropertyValue());
							if(up.get(i).getProperty().getPropertyName().equals("toeic_score"))
								x.get(j).setToeic_score(up.get(i).getPropertyValue());
							if(up.get(i).getProperty().getPropertyName().equals("experience"))
								x.get(j).setExperience(up.get(i).getPropertyValue());
							if(up.get(i).getProperty().getPropertyName().equals("department"))
								x.get(j).setDepartment(up.get(i).getPropertyValue());
							if(up.get(i).getProperty().getPropertyName().equals("location"))
								x.get(j).setLocation(up.get(i).getPropertyValue());
							
							x.add(a);
							break;
						}
					}
			}				
		}
		
		model.addAttribute("listUserProperty",x);
		model.addAttribute("name", x.get(0).getName());
		model.addAttribute("age", x.get(0).getAge());
		model.addAttribute("date_of_birth", x.get(0).getDate_of_birth());
		model.addAttribute("education", x.get(0).getEducation());
		model.addAttribute("main_program_language", x.get(0).getMain_program_language());
		model.addAttribute("toeic_score", x.get(0).getToeic_score());
		model.addAttribute("experience", x.get(0).getExperience());
		model.addAttribute("department",x.get(0).getDepartment());
		model.addAttribute("location", x.get(0).getLocation());
		
		
		model.addAttribute("listUser",l);
		
		return "training_editproperty_trainee";
	}
	
	@RequestMapping(value="/xulysearch")
	public String xulysearch(@RequestParam("search") String search,@RequestParam("colum") String colum,Model model){
		
		if(search.equals(""))
		{
			return "redirect:/formtrainingpropertytrainee/training_addproperty_trainee";
		}
		
		
		model.addAttribute("userPropertyId",new UserpropertyId());
		model.addAttribute("userProperty",new Userproperty());
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		
		List<Userproperty> l3 = new ArrayList<>();
		l3 = userProperty.findAllByPropertyPropertyNameAndPropertyValue(colum, search);
		
		List<Userproperty> up = new ArrayList<Userproperty>();
		List<Userproperty> up1 = new ArrayList<Userproperty>();
		up1 = userProperty.findAllByUserJob(job.findByJobName("trainee"));
		
		for(int i = 0 ; i < up1.size() ;i++){
			for(int j = 0 ; j < l3.size() ;j++)
			{				
				if(up1.get(i).getUser().getAccount().equals(l3.get(j).getUser().getAccount()))
					{
						up.add(up1.get(i));
						break;
					}
			}
		}
		
		
		List<ClassTraineeProfile> x = new ArrayList<>();
		if(!up.isEmpty()) // khac null moi show ra
		for(int i = 0; i < up.size() ; i++)
		{
			if(x.isEmpty()) // truong hop dau tien rong thi tao moi
			{
				ClassTraineeProfile a = new ClassTraineeProfile();
				a.setAccount(up.get(i).getUser().getAccount());
				a.setUserid(user.findByAccount(a.getAccount()).getUserId());
				x.add(a);
			}
			for(int j = 0 ; j < x.size() ;j++)
			{
				if(up.get(i).getUser().getAccount().equals(x.get(j).getAccount()))
				{
					if(up.get(i).getProperty().getPropertyName().equals("name"))
						x.get(j).setName(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("age"))
						x.get(j).setAge(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("date_of_birth"))
						x.get(j).setDate_of_birth(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("education"))
						x.get(j).setEducation(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("main_program_language"))
						x.get(j).setMain_program_language(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("toeic_score"))
						x.get(j).setToeic_score(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("experience"))
						x.get(j).setExperience(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("department"))
						x.get(j).setDepartment(up.get(i).getPropertyValue());
					if(up.get(i).getProperty().getPropertyName().equals("location"))
						x.get(j).setLocation(up.get(i).getPropertyValue());
					break;
				}
				else
				{ // khong tim duoc thi tao moi va them vo
					if(!up.get(i).getUser().getAccount().equals(x.get(j).getAccount()) && j == x.size()-1)
					{
						ClassTraineeProfile a = new ClassTraineeProfile();
						a.setAccount(up.get(i).getUser().getAccount());
						a.setUserid(user.findByAccount(a.getAccount()).getUserId());
						
						if(up.get(i).getProperty().getPropertyName().equals("name"))
							a.setName(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("age"))
							a.setAge(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("date_of_birth"))
							a.setDate_of_birth(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("education"))
							a.setEducation(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("main_program_language"))
							a.setMain_program_language(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("toeic_score"))
							a.setToeic_score(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("experience"))
							a.setExperience(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("department"))
							a.setDepartment(up.get(i).getPropertyValue());
						if(up.get(i).getProperty().getPropertyName().equals("location"))
							a.setLocation(up.get(i).getPropertyValue());
						
						x.add(a);
						break;
					}
				}
			}				
		}
		model.addAttribute("listUserProperty",x);

		return "training_listtrainee_search";
	}
	
//	@RequestMapping(value="/xulysua")
//	public String xulysua(@ModelAttribute Userproperty u){
//		userProperty.sua(u);
//		return "redirect:/formtrainingpropertytrainee/training_editproperty_trainee";
//	}
	
	
}

