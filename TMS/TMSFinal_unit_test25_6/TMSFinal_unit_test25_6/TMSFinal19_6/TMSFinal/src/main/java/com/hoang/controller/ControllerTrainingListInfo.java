package com.hoang.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hoang.domain.Job;
import com.hoang.domain.Property;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;
import com.hoang.domain.training_info;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceProperty;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;





@Controller
@RequestMapping(value="/formtraining_info")
public class ControllerTrainingListInfo {

	
	 @Autowired
	 ServiceProperty property;
	 @Autowired
	 ServiceUser user;
	 @Autowired
	ServiceJob Job;
	 @Autowired
	ServiceUserProperty servicePro;
	 
	@RequestMapping(value="/listTrainingInfoTrainee")
	public String them(Model model){
		List <User> l = new ArrayList<>();
		Job job=Job.findByJobName("trainer");
		l=user.findAllByJob(job);
		model.addAttribute("listtrainee", l);
		model.addAttribute("user",new User());
		
		
		return "listTrainingInfoTrainee";
	}
	@RequestMapping(value="/xulythem/{id}")
	public String xulythem(@PathVariable String id, Model model){
	training_info tra_info = new training_info();
	tra_info.setUser(user.findOneSV(Integer.parseInt(id)));
	List <Userproperty> l= servicePro.findAllByUser(tra_info.getUser());
	if(!l.isEmpty())
	{
		for(int i = 0 ; i < l.size() ;i++)
		{
				if(l.get(i).getProperty().getPropertyName().equals("name"))
					tra_info.setName(l.get(i).getPropertyValue());
				if(l.get(i).getProperty().getPropertyName().equals("type"))
					 tra_info.setType(l.get(i).getPropertyValue());
				if(l.get(i).getProperty().getPropertyName().equals("education"))
					tra_info.setEducation(l.get(i).getPropertyValue());
				if(l.get(i).getProperty().getPropertyName().equals("working_place"))
					tra_info.setWorking_place(l.get(i).getPropertyValue());
				if(l.get(i).getProperty().getPropertyName().equals("telephone"))
					tra_info.setTelephone(l.get(i).getPropertyValue());
				if(l.get(i).getProperty().getPropertyName().equals("email"))
					tra_info.setEmail(l.get(i).getPropertyValue());

		 }	
	}
	model.addAttribute("info",tra_info);
	model.addAttribute("user", tra_info.getUser());
	return 	"infoTrainer";
}
	@RequestMapping(value="/dathem")
	public String dathem(@ModelAttribute training_info t){
		Userproperty userPro= new Userproperty();
		Property pro_temp= new Property();
		UserpropertyId proid_temp=new UserpropertyId();
		userPro.setUser(user.findOneSV(t.getUser().getUserId()));
		
		
		userPro.setPropertyValue(t.getName());
		pro_temp=property.findByPropertyName("name");
		userPro.setProperty(pro_temp);
		int userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
	   Integer prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
	
		
		userPro.setPropertyValue(t.getType());
		pro_temp=property.findByPropertyName("type");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getEducation());
		pro_temp=property.findByPropertyName("education");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		
		userPro.setPropertyValue(t.getWorking_place());
		pro_temp=property.findByPropertyName("working_place");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getTelephone());
		pro_temp=property.findByPropertyName("telephone");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getEmail());
		pro_temp=property.findByPropertyName("email");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		return "redirect:/formtraining_info/listTrainingInfoTrainee";
	}
	@RequestMapping(value="/xoa/{id}")
	public String xoa(@PathVariable String id){
		User a=user.findOneSV(Integer.parseInt(id));
		List<Userproperty> l=servicePro.findAllByUser(a);
		for(int i=0;i<l.size();i++)
		{
			Userproperty temp=l.get(i);
			servicePro.delete(temp);
		}
		return "redirect:/formtraining_info/listTrainingInfoTrainee";
	}
	@RequestMapping(value="/xoa1/{id}")
	public String xoa1(@PathVariable String id){
		User a=user.findOneSV(Integer.parseInt(id));
		List<Userproperty> l=servicePro.findAllByUser(a);
		for(int i=0;i<l.size();i++)
		{
			Userproperty temp=l.get(i);
			servicePro.delete(temp);
		}
		return "redirect:/formtraining_info/listTrainingInfoTrainee1";
	}
	@RequestMapping(value="/listTrainingInfoTrainee1")
	public String them1(Model model){
		List <User> l = new ArrayList<>();
		Job job=Job.findByJobName("trainee");
		l=user.findAllByJob(job);
		model.addAttribute("listtrainee", l);
		model.addAttribute("user",new User());
		
		List<Property> l2 = new ArrayList<>();
		l2.add(property.findByPropertyName("name"));
		l2.add(property.findByPropertyName("age"));
		l2.add(property.findByPropertyName("date_of_birth"));
		l2.add(property.findByPropertyName("education"));
		l2.add(property.findByPropertyName("main_program_language"));
		l2.add(property.findByPropertyName("toeic_score"));
		l2.add(property.findByPropertyName("experience"));
		l2.add(property.findByPropertyName("department"));
		l2.add(property.findByPropertyName("location"));
		l2.add(property.findByPropertyName("working_place"));
		l2.add(property.findByPropertyName("telephone"));
		l2.add(property.findByPropertyName("email"));
		
		model.addAttribute("listProperty",l2);
		
		return "listTrainingInfoTrainee1";
	}
	@RequestMapping(value="/xulythem1/{id}")
	public String xulythem1(@PathVariable String id, Model model){
		training_info tra_info = new training_info();
		tra_info.setUser(user.findOneSV(Integer.parseInt(id)));
		List <Userproperty> l= servicePro.findAllByUser(tra_info.getUser());
		if(!l.isEmpty())
		{
			for(int i = 0 ; i < l.size() ;i++)
			{
					if(l.get(i).getProperty().getPropertyName().equals("name"))
						tra_info.setName(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("age"))
						 tra_info.setAge(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("date_of_birth"))
						tra_info.setDate_of_birth(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("education"))
						 tra_info.setEducation(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("main_program_language"))
						 tra_info.setMain_program_language(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("toeic_score"))
						 tra_info.setToeic_score(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("experience"))
						 tra_info.setExperience(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("department"))
						 tra_info.setDepartment(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("location"))
						 tra_info.setLocation(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("working_place"))
						tra_info.setWorking_place(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("telephone"))
						tra_info.setTelephone(l.get(i).getPropertyValue());
					if(l.get(i).getProperty().getPropertyName().equals("email"))
						tra_info.setEmail(l.get(i).getPropertyValue());				
				}
		}
			 
		model.addAttribute("info",tra_info);
		model.addAttribute("user", tra_info.getUser());
	return 	"infoTrainer1";
}
	@RequestMapping(value="/dathem1")
	public String dathem1(@ModelAttribute training_info t){
		Userproperty userPro= new Userproperty();
		Property pro_temp= new Property();
		UserpropertyId proid_temp=new UserpropertyId();
		userPro.setUser(user.findOneSV(t.getUser().getUserId()));
		
		
		userPro.setPropertyValue(t.getName());
		pro_temp=property.findByPropertyName("name");
		userPro.setProperty(pro_temp);
		int userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
	   Integer prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getAge());
		pro_temp=property.findByPropertyName("age");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getDate_of_birth());
		pro_temp=property.findByPropertyName("date_of_birth");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getEducation());
		pro_temp=property.findByPropertyName("education");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getMain_program_language());
		pro_temp=property.findByPropertyName("main_program_language");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getToeic_score());
		pro_temp=property.findByPropertyName("toeic_score");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getExperience());
		pro_temp=property.findByPropertyName("experience");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getDepartment());
		pro_temp=property.findByPropertyName("department");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getLocation());
		pro_temp=property.findByPropertyName("location");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getWorking_place());
		pro_temp=property.findByPropertyName("working_place");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getTelephone());
		pro_temp=property.findByPropertyName("telephone");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		
		userPro.setPropertyValue(t.getEmail());
		pro_temp=property.findByPropertyName("email");
		userPro.setProperty(pro_temp);
	    userid=user.findOneSV(t.getUser().getUserId()).getUserId();
		proid_temp.setUserId(userid);
		prop_id=pro_temp.getPropertyId();
		proid_temp.setPropertyId(prop_id);
		userPro.setId(proid_temp);
		servicePro.them(userPro);
		return "redirect:/formtraining_info/listTrainingInfoTrainee1";
	}
	@RequestMapping(value="/xulysearch")
	public String xulysearch(@RequestParam("search") String search,@RequestParam("colum") String colum,Model model){
		
		if(search.equals(""))
		{
			return "redirect:/formtraining_info/listTrainingInfoTrainee1";
		}
		
		List<Userproperty> l3 = new ArrayList<>();
		l3 = servicePro.findAllByPropertyPropertyNameAndPropertyValue(colum, search);
		
		model.addAttribute("listtrainee", l3);
		
		model.addAttribute("userPropertyId",new UserpropertyId());
		model.addAttribute("userProperty",new Userproperty());
		model.addAttribute("user",new User());
		model.addAttribute("property",new Property());
		
		

		return "training_listtrainee_search";
	}
	

}
