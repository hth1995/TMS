package com.hoang.DAO;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hoang.domain.Permission;

@Transactional
public interface DAOPermission extends JpaRepository<Permission, Integer>{

}
