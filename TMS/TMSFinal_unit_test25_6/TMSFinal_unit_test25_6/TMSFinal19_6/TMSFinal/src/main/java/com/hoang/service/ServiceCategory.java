package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOCategory;
import com.hoang.domain.Category;

@Service
public class ServiceCategory {
	@Autowired
	DAOCategory service;
	
	public void them(Category ct){
		if(service.findByCategoryName(ct.getCategoryName()) == null)
			service.save(ct);
	}
	
	public void xoa(int id){
		service.delete(id);
	}
	
	public Category findOneSV(int id){
		return service.findOne(id);
	}
	
	public void sua(Category ct){
		Category temp = service.findOne(ct.getCategoryId());
		temp.setCategoryId(ct.getCategoryId());
		temp.setCategoryName(ct.getCategoryName());
		temp.setDescription(ct.getDescription());
		service.save(temp);
	}

	public List<Category> lietKe(){
		return service.findAll();
	}
	
	public List<Category> findAllByCategoryNameOrDescription(String search){
		return service.findAllByCategoryNameOrDescription(search);
	}
} 
