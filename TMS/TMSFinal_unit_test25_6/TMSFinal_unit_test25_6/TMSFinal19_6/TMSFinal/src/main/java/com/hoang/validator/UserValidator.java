package com.hoang.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hoang.domain.User;
import com.hoang.service.ServiceUser;

@Component
public class UserValidator implements Validator {
    @Autowired
    private ServiceUser userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        @SuppressWarnings("unused")
		User user = (User) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "account", "NotEmpty.user.account");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.user.password");
      
    }
}