package com.hoang.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hoang.domain.Job;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;

@Transactional
public interface DAOUserProperty extends JpaRepository<Userproperty, UserpropertyId>{
	
	Userproperty findAllByIdPropertyIdAndIdUserId(Integer idPropertyId,Integer idUserId);
	
	List<Userproperty> findAllByUserJob(Job job);
	List<Userproperty> findAllByUserAccount(String account);
	List <Userproperty> findAllByUser (User a);
	@Query("SELECT up FROM Userproperty up where up.property.propertyName = :colum AND up.propertyValue like :search ")
	List<Userproperty> findAllByPropertyPropertyNameAndPropertyValue( @Param("colum") String colum, @Param("search") String search);
	 
}
