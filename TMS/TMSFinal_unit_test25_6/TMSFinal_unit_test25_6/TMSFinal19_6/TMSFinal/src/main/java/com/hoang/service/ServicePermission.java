package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOPermission;
import com.hoang.domain.Permission;

@Service
public class ServicePermission {

	@Autowired
	DAOPermission service;
	
	public void them(Permission pm){
		service.save(pm);
	}
	
	public void xoa(int id){
		service.delete(id);
	}
	
	public Permission findOneSV(int id){
		return service.findOne(id);
	}
	
	public void sua(Permission pm){
		Permission temp = service.findOne(pm.getPermissionId());
		temp.setPermissionId(pm.getPermissionId());
		temp.setPermissionName(pm.getPermissionName());
		temp.setDescription(pm.getDescription());
		service.save(temp);
	}
	
	public List<Permission> lietKe(){
		return service.findAll();
	}
}
