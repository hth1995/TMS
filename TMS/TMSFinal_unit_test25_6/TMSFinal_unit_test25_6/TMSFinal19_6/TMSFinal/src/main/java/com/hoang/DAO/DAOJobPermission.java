package com.hoang.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hoang.domain.Job;
import com.hoang.domain.JobPermission;
import com.hoang.domain.JobPermissionId;

@Transactional
public interface DAOJobPermission extends JpaRepository<JobPermission, JobPermissionId>{
	
	List<JobPermission> findAllByJob(Job job);

}
