package com.hoang.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hoang.domain.Topic;
import com.hoang.domain.User;

@Transactional
public interface DAOTopic extends JpaRepository<Topic, Integer>{
	Topic findByTopicName(String topicName);
	List<Topic> findAllByUserAccount(String account);
	List <Topic> findAllByUser (User a);
	Topic findByTopicId (Integer id);
	List <Topic> findAllByCourseCourseId (int id);
	@Query("SELECT c FROM Topic c where c.topicName like %:search% OR c.description like %:search%")
	List<Topic> findAllByTopicNameOrDescription(@Param("search") String search);
}
