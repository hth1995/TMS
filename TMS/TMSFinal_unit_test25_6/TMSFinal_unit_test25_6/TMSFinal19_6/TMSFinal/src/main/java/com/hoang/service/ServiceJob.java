package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOJob;
import com.hoang.domain.Job;

@Service
public class ServiceJob {
	
	@Autowired
	DAOJob service;
	
	public void them(Job j){
		if(service.findByJobName(j.getJobName()) == null)
		service.save(j);
	}
	
	public void xoa(int id){
		service.delete(id);
	}
	
	public Job findOneSV(int id){
		return service.findOne(id);
	}
	
	public void sua(Job j){
		Job temp = service.findOne(j.getJobId());
		temp.setJobId(j.getJobId());
		temp.setJobName(j.getJobName());
		temp.setDescription(j.getDescription());
		service.save(temp);
	}
	
	public Job findByJobName(String Job){
		return service.findByJobName(Job);
	}
	
	public List<Job> lietKe(){
		return service.findAll();
	}

}
