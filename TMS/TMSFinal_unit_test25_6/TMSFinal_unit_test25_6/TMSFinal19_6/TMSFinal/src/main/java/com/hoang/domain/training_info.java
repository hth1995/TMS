package com.hoang.domain;


public class training_info {
	private String account;
	private int userid;
	
	private String name;
	
    private String age;
	
    private String date_of_birth;
	
    private String education;

    private String main_program_language;
	
    private String toeic_score;
	
    private String experience;
	
    private String department;
	
    private String location;
	
    private String working_place;
	
    private String telephone;
	
    private String email; 
    
    private String type;
    
    
   
    public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	private User user;
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getToeic_score() {
		return toeic_score;
	}
	public void setToeic_score(String toeic_score) {
		this.toeic_score = toeic_score;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getWorking_place() {
		return working_place;
	}
	public void setWorking_place(String working_place) {
		this.working_place = working_place;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getMain_program_language() {
		return main_program_language;
	}
	public void setMain_program_language(String main_program_language) {
		this.main_program_language = main_program_language;
	}
	
	
	public training_info(String account, int userid, String name, String age, String date_of_birth, String education,
			String main_program_language, String toeic_score, String experience, String department, String location,
			String working_place, String telephone, String email, String type, User user) {
		super();
		this.account = account;
		this.userid = userid;
		this.name = name;
		this.age = age;
		this.date_of_birth = date_of_birth;
		this.education = education;
		this.main_program_language = main_program_language;
		this.toeic_score = toeic_score;
		this.experience = experience;
		this.department = department;
		this.location = location;
		this.working_place = working_place;
		this.telephone = telephone;
		this.email = email;
		this.type = type;
		this.user = user;
	}

	public training_info() {
		super();
	}
    
    
    
}
