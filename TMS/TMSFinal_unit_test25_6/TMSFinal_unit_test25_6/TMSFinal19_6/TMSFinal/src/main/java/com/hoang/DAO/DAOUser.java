package com.hoang.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hoang.domain.Job;
import com.hoang.domain.User;

@Transactional
public interface DAOUser extends JpaRepository<User, Integer> {

	User findByAccount(String account);
	
	List<User> findAllByJob(Job job);
	
}
