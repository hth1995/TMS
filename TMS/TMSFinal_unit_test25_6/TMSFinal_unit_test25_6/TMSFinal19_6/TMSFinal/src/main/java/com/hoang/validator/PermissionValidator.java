package com.hoang.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.hoang.domain.Permission;
import com.hoang.service.ServicePermission;

@Component
public class PermissionValidator implements Validator{

	
	@Autowired
	 private ServicePermission servicePermission;
	
	
	  @Override
	    public boolean supports(Class<?> aClass) {
	        return Permission.class.equals(aClass);
	    }
	  

	  @Override
	    public void validate(Object o, Errors errors) {
		  @SuppressWarnings("unused")
		Permission permission = (Permission) o;

	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "permissionName", "NotEmpty.permission.permissionName");
	        
	      
	    }
}
