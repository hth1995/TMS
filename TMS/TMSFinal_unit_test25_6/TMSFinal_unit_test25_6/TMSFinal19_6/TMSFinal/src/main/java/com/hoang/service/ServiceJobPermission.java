package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOJobPermission;
import com.hoang.domain.Job;
import com.hoang.domain.JobPermission;

@Service

public class ServiceJobPermission {
	
	@Autowired
	DAOJobPermission service;

	public List<JobPermission> findAllByJob(Job a)
	{
		return service.findAllByJob(a);
	}
	
}
