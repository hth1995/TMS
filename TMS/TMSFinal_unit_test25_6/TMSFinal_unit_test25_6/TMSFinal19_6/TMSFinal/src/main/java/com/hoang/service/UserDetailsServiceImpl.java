package com.hoang.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hoang.DAO.DAOJobPermission;
import com.hoang.DAO.DAOUser;
import com.hoang.domain.Job;
import com.hoang.domain.JobPermission;
import com.hoang.domain.User;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private DAOUser userRepository;
    
    @Autowired
    private DAOJobPermission jobPermission;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByAccount(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        Job job = user.getJob();
        List <JobPermission> arr = jobPermission.findAllByJob(job);
        
        grantedAuthorities.add(new SimpleGrantedAuthority(job.getJobName()));
        
        for(int i = 0 ; i < arr.size() ; i++)
        {
        	grantedAuthorities.add(new SimpleGrantedAuthority(arr.get(i).getPermission().getPermissionName()));
        }
        
        
        return new org.springframework.security.core.userdetails.User(
                user.getAccount(), user.getPassword(), grantedAuthorities);
    }

}