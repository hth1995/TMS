package com.hoang.config;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration // lop de cau hinh
@EnableWebSecurity // kich hoat viec tich hop spring security voi spring mvc
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/register").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/formadmin/admin_adduser_trainer").hasAuthority("Manage Trainer Account")
                .antMatchers("/formadmintraining/admin_adduser_training").hasAuthority("Manage Training Account")
                
                
                
                .antMatchers("/formcourse/themcourse").hasAuthority("Manage course")
                .antMatchers("/formtopic/themtopic").hasAuthority("Manage topic")
                .antMatchers("/formtraining/training_adduser_trainee").hasAuthority("Manage trainee account")
                .antMatchers("/formtraining/training_edituser_trainee").hasAuthority("Manage trainee account")
                .antMatchers("/formtraining_info/listTrainingInfoTrainee").hasAuthority("Manage trainer profile")
                .antMatchers("/formtraining_info/listTrainingInfoTrainee1").hasAuthority("Manage trainee profile")
                .antMatchers("/formcourseuser/training_addcourse_trainee").hasAuthority("Add course")
                .antMatchers("/formcourseuser/training_editcourse_trainee").hasAuthority("Add course")
                .antMatchers("/form/them").hasAuthority("Manage category")
                .antMatchers("/trainer").hasAuthority("Import list trainee accounts")
                
                
                .antMatchers("/formtrainer/trainer_update_info").hasAuthority("Update profile")
                .antMatchers("/formtrainerviewcourse/trainer_viewcourse").hasAuthority("View course topic assigned to")
                
                .and()
            .formLogin()
                .loginPage("/login")
                .usernameParameter("account")
                .passwordParameter("password")
                .defaultSuccessUrl("/index")
                .failureUrl("/login?error")
                .and()
            .exceptionHandling()
                .accessDeniedPage("/403");
    }

}