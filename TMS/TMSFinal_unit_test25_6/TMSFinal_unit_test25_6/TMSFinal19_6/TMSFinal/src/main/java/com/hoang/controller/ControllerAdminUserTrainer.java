package com.hoang.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
//import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
	//import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.domain.Job;
import com.hoang.domain.Property;
import com.hoang.domain.Topic;
import com.hoang.domain.User;
import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;
import com.hoang.service.ServiceJob;
import com.hoang.service.ServiceProperty;
import com.hoang.service.ServiceTopic;
import com.hoang.service.ServiceUser;
import com.hoang.service.ServiceUserProperty;
import com.hoang.validator.UserValidator;
//import javax.validation.Valid;





@Controller
@RequestMapping(value="/formadmin")
public class ControllerAdminUserTrainer  {

	@Autowired
	ServiceJob job;
	@Autowired
	ServiceUser user;
	@Autowired
	ServiceUserProperty userproperty;
	@Autowired
	ServiceProperty property;
	@Autowired
	UserValidator userValidator;
	@Autowired
	ServiceTopic topic;
	 @InitBinder
	   protected void initBinder(WebDataBinder dataBinder) {
	   
	       // Form mục tiêu
	       Object target = dataBinder.getTarget();
	       if (target == null) {
	           return;
	       }
	       System.out.println("Target=" + target);
	 
	       if (target.getClass() == User.class) {
	           dataBinder.setValidator(userValidator);
	       }
	   }
	
	@RequestMapping(value="/admin_adduser_trainer")
	public String them(Model model, @Validated User us, BindingResult result, final RedirectAttributes redirectAttributes){
		
		model.addAttribute("user",new User());
		model.addAttribute("job",new Job());
		Job temp = job.findByJobName("trainer");
		List<Job> l = new ArrayList<Job>();
		l.add(temp);
		model.addAttribute("listJob",l);
		List<User> l2 = new ArrayList<User>();
		l2=user.findAllByJob(temp);
		model.addAttribute("listUser",l2);
		
		return "admin_adduser_trainer";
	}

//	@RequestMapping(value="/xuly",method=RequestMethod.POST)
//	public String xuly(@Valid User us ,BindingResult bindingResult){
//		 if(bindingResult.hasErrors()){
//       	  return "admin_adduser_trainer";
//         }
//	      
//	        return "/formadmin/admin_adduser_trainer";
//		
//		
//	}
	@RequestMapping(value="/xulythem",method=RequestMethod.POST)
	public String xulythem(Model model,@ModelAttribute("user") @Validated User us, BindingResult result, final RedirectAttributes redirectAttributes){
		//userValidator.validate(us, result);
		 if(result.hasErrors()){
			 return "admin_adduser_trainer";	
		 }
		 user.them(us); 
		 return "redirect:/formadmin/admin_adduser_trainer";
		
	}

	


	public void addPropertyTrainer(User u){
		User temp = user.findByAccount(u.getAccount());
		Integer iduser = temp.getUserId();
		
		UserpropertyId upid = new UserpropertyId();
		Userproperty up = new Userproperty();
		
		Property puser = property.findByPropertyName("name");
		upid.setPropertyId(puser.getPropertyId());
		upid.setUserId(iduser);		
		up.setId(upid);
		userproperty.them(up);
		
		puser = property.findByPropertyName("external");
		upid.setPropertyId(puser.getPropertyId());
		upid.setUserId(iduser);		
		up.setId(upid);
		userproperty.them(up);
		
		puser = property.findByPropertyName("internal");
		upid.setPropertyId(puser.getPropertyId());
		upid.setUserId(iduser);		
		up.setId(upid);
		userproperty.them(up);
		
		puser = property.findByPropertyName("working_place");
		upid.setPropertyId(puser.getPropertyId());
		upid.setUserId(iduser);		
		up.setId(upid);
		userproperty.them(up);
		
		puser = property.findByPropertyName("telephone");
		upid.setPropertyId(puser.getPropertyId());
		upid.setUserId(iduser);		
		up.setId(upid);
		userproperty.them(up);
		
		puser = property.findByPropertyName("email");
		upid.setPropertyId(puser.getPropertyId());
		upid.setUserId(iduser);		
		up.setId(upid);
		userproperty.them(up);
	}
	

	
//	@RequestMapping(value="/xoauser/{id}")
//	public String xoa(@PathVariable String id){
//		user.xoa(Integer.parseInt(id));
//		return "redirect:/formadmin/admin_adduser_trainer";
//	}
//	

	@RequestMapping(value="/xoauser/{id}")
	public String xoa(@PathVariable String id){
		User a= user.findOneSV(Integer.parseInt(id));
		List<Topic> l= topic.findAllByUsername(a);
		if(l.size()!=0)
		{
			return "403";
		}
		else
		{
			List<Userproperty> pro = userproperty.findAllByUser(a);
			for(int i=0; i<pro.size();i++)
			{
				userproperty.delete(pro.get(i));
			}
			user.xoa(Integer.parseInt(id));
		return "redirect:/formadmin/admin_adduser_trainer";
		}
	}
	@RequestMapping(value="/admin_edituser_trainer/{id}")
	public String sua(@PathVariable String id,Model model){
		
		model.addAttribute("user",user.findOneSV(Integer.parseInt(id)));
		model.addAttribute("job", new Job());
		List<Job> l = new ArrayList<Job>();
		l.add(job.findByJobName("trainer"));
		l.add(job.findByJobName("training"));
		model.addAttribute("listJob",l);
		model.addAttribute("listUser",user.findAllByJob(job.findByJobName("trainer")));
		return "admin_edituser_trainer";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute User u){
		user.sua(u);
		return "redirect:/formadmin/admin_adduser_trainer";
	}
	
	@RequestMapping(value="/xulylogout")
	public String logout(HttpSession session){
		session.removeAttribute("loggedInUser");
		return "redirect:/formlogin/login";
	}

//	@PostMapping("/formadmin/admin_adduser_trainer/luu")
//	public String luu(@Valid User user1, BindingResult result, RedirectAttributes redirect) {
//	    if (result.hasErrors()) {
//	        return "/formadmin/admin_adduser_trainer";
//	    }
//	    user.save(user1);
//	    redirect.addFlashAttribute("success", "Saved  successfully!");
//	    return "redirect:/formadmin";
//	}

		
	
}
