package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOTopic;
import com.hoang.domain.Topic;
import com.hoang.domain.User;

@Service
public class ServiceTopic {
	@Autowired
	DAOTopic service;
	
	
	public void them(Topic tp){
		if(service.findByTopicName(tp.getTopicName()) == null)
		service.save(tp);
	}
	
	public void xoa(int id){
		service.delete(id);
	}
	
	public Topic findOneSV(int id){
		return service.findOne(id);
	}
	
	public Topic findById(int id){
		return service.findByTopicId(id);
	}
	public void sua(Topic tp){
		Topic temp = service.findOne(tp.getTopicId());
		temp.setTopicId(tp.getTopicId());
		temp.setTopicName(tp.getTopicName());
		temp.setDescription(tp.getDescription());
		temp.setUser(tp.getUser());
		temp.setCourse(tp.getCourse());
		service.save(temp);
	}

	public List<Topic> lietKe(){
		return service.findAll();
	}
	
	public List<Topic> findAllByUserAccount(String account){
		return service.findAllByUserAccount(account);
	}
	
	public List <Topic> findAllByUsername (User a){
		return service.findAllByUser(a);
	}
	
	public List <Topic> findAllByCourseCourseId (int id){
		return service.findAllByCourseCourseId(id);
	}
	public List<Topic> findAllByTopicNameOrDescription(String search){
		return service.findAllByTopicNameOrDescription(search);
	}
}
