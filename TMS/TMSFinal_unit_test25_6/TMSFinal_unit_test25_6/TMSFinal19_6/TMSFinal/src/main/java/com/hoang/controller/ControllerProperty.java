package com.hoang.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hoang.domain.Property;
import com.hoang.domain.User;
import com.hoang.service.ServiceProperty;
import com.hoang.validator.PropertyValidator;

@Controller
@RequestMapping(value="/formproperty")

public class ControllerProperty {
	
	@Autowired
	ServiceProperty property;
	@Autowired
	PropertyValidator propertyValidator;
	
	 @InitBinder
	   protected void initBinder(WebDataBinder dataBinder) {
	   
	       // Form mục tiêu
	       Object target = dataBinder.getTarget();
	       if (target == null) {
	           return;
	       }
	       System.out.println("Target=" + target);
	 
	       if (target.getClass() == User.class) {
	           dataBinder.setValidator(propertyValidator);
	       }
	   }
	
	@RequestMapping(value="/themproperty")
	public String them(Model model){
		model.addAttribute("property",new Property());
		model.addAttribute("listProperty",property.lietKe());
		return "themproperty";
	}
	
//	@RequestMapping(value="/xulythem")
//	public String xulythem(@ModelAttribute Property ct){
//		property.them(ct);
//		return "redirect:/formproperty/themproperty";
//	}
	
	@RequestMapping(value="/xulythem",method=RequestMethod.POST)
	public String xulythem(Model model,@ModelAttribute("property") @Validated Property ct, BindingResult result, final RedirectAttributes redirectAttributes){
		//userValidator.validate(us, result);
		 if(result.hasErrors()){
			 return "themproperty";	
		 }
		 property.them(ct); 
		 return "redirect:/formproperty/themproperty";
		
	}
	@RequestMapping(value="/xoaproperty/{id}")
	public String xoa(@PathVariable String id){
		property.xoa(Integer.parseInt(id));
		return "redirect:/formproperty/themproperty";
	}
	
	@RequestMapping(value="/suaproperty/{id}")
	public String sua(@PathVariable String id,Model model){
		model.addAttribute("property",property.findOneSV(Integer.parseInt(id)));
		model.addAttribute("listProperty",property.lietKe());
		return "suaproperty";
	}
	
	@RequestMapping(value="/xulysua")
	public String xulysua(@ModelAttribute Property ct){
		property.sua(ct);
		return "redirect:/formproperty/themproperty";
	}


}
