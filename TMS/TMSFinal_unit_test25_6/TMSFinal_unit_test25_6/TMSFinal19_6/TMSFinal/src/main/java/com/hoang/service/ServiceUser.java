package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hoang.DAO.DAOUser;
import com.hoang.domain.Job;
import com.hoang.domain.User;


@Service
public class ServiceUser {
	
	@Autowired
	DAOUser service;
	
	@Autowired 
    private PasswordEncoder passwordEncoder;
	
	public void them(User user){
		if(service.findByAccount(user.getAccount()) == null)
		{
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			service.save(user);
		}
	}
	
	public void xoa(int id){
		service.delete(id);
	}
	
	
  
	public User findOneSV(int id){
		return service.findOne(id);
	}
	
	public void sua(User user){
	//	User userPer = service.findOne(user.getUserId());
		User temp = user;
		if(!temp.getPassword().equals(""))
		{
			temp.setUserId(user.getUserId());
			temp.setAccount(user.getAccount());
			temp.setPassword(passwordEncoder.encode(user.getPassword()));
			temp.setJob(user.getJob());
		
			service.save(temp);
		}
	}
	
	public List<User> findAllByJob(Job job){
		return service.findAllByJob(job);
	}

	public List<User> lietKe(){
		return service.findAll();
	}
	
	public User findByAccount(String account){
		return service.findByAccount(account);
	}
	
	@Transactional
	public User save(User category) {
		return service.save(category);
	}
	
//	public User loginUser(String userId,String password){
//		User user = this.findOneSV(Integer.parseInt(userId));
//		if(user != null && user.getPassword().equals(password))
//		{
//			return user;
//		}
//		return null;
//	}

}
