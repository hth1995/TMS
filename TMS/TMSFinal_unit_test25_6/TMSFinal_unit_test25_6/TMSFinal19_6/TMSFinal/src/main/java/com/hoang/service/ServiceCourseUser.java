package com.hoang.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hoang.DAO.DAOCourseUser;
import com.hoang.domain.CourseUser;
import com.hoang.domain.CourseUserId;

@Service
public class ServiceCourseUser {

	@Autowired
	DAOCourseUser service;
	
	public void them(CourseUser up){
		service.save(up);
	}
	
	public void xoa(int idCourseId,int idUserId){
		service.delete(service.findAllByIdCourseIdAndIdUserId(idCourseId, idUserId));
	}
	
	public CourseUser findOneSV(CourseUserId id){
		return service.findOne(id);
	}
	
	public CourseUser findAllByIdCourseIdAndIdUserId(int idPropertyId,int idUserId){
		return service.findAllByIdCourseIdAndIdUserId(idPropertyId, idUserId);
	}
	
	public void sua(CourseUser tp){
		CourseUser temp = service.findOne(tp.getId());
		temp.setCourse(tp.getCourse());
		temp.setUser(tp.getUser());
		service.save(temp);
	}

	public List<CourseUser> lietKe(){
		return service.findAll();
	}
}
