package com.hoang.domain;
// Generated May 26, 2017 3:32:03 PM by Hibernate Tools 5.2.3.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Userproperty generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "userproperty",  catalog = "tms2")
public class Userproperty implements java.io.Serializable {

	private UserpropertyId id;
	private Property property;
	private User user;
	private String propertyValue;

	public Userproperty() {
	}

	public Userproperty(UserpropertyId id, Property property, User user) {
		this.id = id;
		this.property = property;
		this.user = user;
	}

	public Userproperty(UserpropertyId id, Property property, User user, String propertyValue) {
		this.id = id;
		this.property = property;
		this.user = user;
		this.propertyValue = propertyValue;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "userId", column = @Column(name = "UserID", nullable = false)),
			@AttributeOverride(name = "propertyId", column = @Column(name = "PropertyID", nullable = false)) })
	public UserpropertyId getId() {
		return this.id;
	}

	public void setId(UserpropertyId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PropertyID", nullable = false, insertable = false, updatable = false)
	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UserID", nullable = false, insertable = false, updatable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
@NotEmpty
	@Column(name = "PropertyValue", length = 45)
	public String getPropertyValue() {
		return this.propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

}
