package com.hoang.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Course;


/**
 * @author XUKAKULL
 * @version 1.0 Jun 21, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceCourseTest {

    @Autowired
    private ServiceCourse courseService;
    @Autowired
    private ServiceTopic topicService;

    @Autowired
    private ServiceCategory categoryService;

    @Autowired
    private ServiceUser userService;

    @Test
    public void getAllCourse() throws Exception {
       java.util.List<Course> courses = courseService.lietKe();
        assertEquals(3, courses.size());
    }

   
    @Test
    public void addCourse() throws Exception {
        Course course = new Course();
        course.setCourseName("English 8");
        course.setDescription("Tieng anh 8");
        course.setCategory(categoryService.findOneSV(1));
        
        courseService.them(course);
        Course courseAdd = courseService.findOneSV(course.getCourseId());
        assertNotNull(courseAdd);
    }

    @Test
    public void findOneCourse() throws Exception {
        Course course = courseService.findOneSV(7);
        assertNotNull(course);
    }

    @Test
    public void updateCourse() throws Exception {
        Course course = courseService.findOneSV(10);
        course.setDescription("ABC");
        courseService.sua(course);
        Course courseUpdated = courseService.findOneSV(course.getCourseId());
        assertEquals("ABC",courseUpdated.getDescription());
    }

  

    @Test
    public void deleteCourse() throws Exception {
        courseService.xoa(10);
        Course course = courseService.findOneSV(10);
        assertNull(course);
    }

    @Test
    public void findAllByCourseNameOrDescription() throws Exception {
       
    java.util.List<Course> course = courseService.findAllByCourseNameOrDescription("Roots");
        assertEquals(0, course.size());
    }
    
    @Test
    public void findbytopic() throws Exception {
       
    java.util.List<Course> course = courseService.findbytopic(topicService.findOneSV(1));
        assertEquals(1, course.size());
    }

}
