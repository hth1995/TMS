package com.hoang.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Userproperty;
import com.hoang.domain.UserpropertyId;


/**
 * @author XUKAKULL
 * @version 1.0 Jun 22, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceUserPropertyTest {
    @Autowired
    private ServiceUserProperty userPropertyService;

    @Autowired
    private ServiceUser userService;
    @Autowired
    private UserpropertyId userIDService;


    @Autowired
    private ServiceProperty propertyService;
    
    @Test
    public void getUserProperty() throws Exception {
        
        Userproperty userProperty = userPropertyService.findAllByIdPropertyIdAndIdUserId(8,13);
        assertEquals("C#", userProperty.getPropertyValue());
    }
    

   

    

  


}
