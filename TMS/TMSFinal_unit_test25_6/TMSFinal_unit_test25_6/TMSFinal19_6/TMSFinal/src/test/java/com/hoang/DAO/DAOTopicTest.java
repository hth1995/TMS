package com.hoang.DAO;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Topic;
/**
 * @author XUKAKULL
 * @version 1.0 Jun 20, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {RepositoryConfiguration.class})
public class DAOTopicTest {
    @Autowired
    private DAOTopic topicDAO;

    @Autowired
    private DAOUser userDAO;
    @Autowired
    private DAOCourse courseDAO;
    
   
    
    /**
     * {Find Topic by AcccountID of User. }
     * @throws Exception
     */
    @Test
    
    public void findAllByUserAccount() throws Exception {
        java.util.List<Topic> topics = topicDAO.findAllByUser(userDAO.getOne(67));
        assertEquals(2, topics.size());
    }

    @Test
    public void findAllByUser() throws Exception {
        java.util.List<Topic> topics = topicDAO.findAllByUser(userDAO.getOne(67));
        assertEquals(2, topics.size());
    }
    
    
    @Test
    public void findByTopicId() throws Exception {
        Topic topics = topicDAO.findByTopicId(11);
        assertEquals("Animal_Dog", topics.getTopicName());
    }
    
    
    /**
     * {Find Topic by Topic's Name. }
     * @throws Exception
     */
    
    @Test
    public void findByTopicName() throws Exception {
        Topic topics = topicDAO.findByTopicName("Find");
        assertEquals("Find", topics.getTopicName());
    }
}
