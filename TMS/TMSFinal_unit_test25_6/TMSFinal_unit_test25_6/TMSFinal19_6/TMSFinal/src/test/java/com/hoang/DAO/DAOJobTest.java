package com.hoang.DAO;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Job;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 20, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {RepositoryConfiguration.class})
public class DAOJobTest {
    @Autowired
    private DAOJob jobDAO;
  
   
    @Test
    public void findByJobName() throws Exception {
        Job job = jobDAO.findByJobName("training");
        assertEquals("training", job.getJobName());
    }
  
}
