package com.hoang.DAO;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Property;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 21, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {RepositoryConfiguration.class})

public class DAOPropertyTest {
    @Autowired
    private DAOProperty proDAO;
    
        @Test
        public void findByPropertyName() throws Exception {
            Property pro = proDAO.findByPropertyName("experience");
            assertEquals("experience", pro.getPropertyName());
        }
      
        @Test
        public void findByPropertyId() throws Exception {
            Property pro = proDAO.findByPropertyId(8);
          assertEquals("name", pro.getPropertyName());
      }

}
