package com.hoang.DAO;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.User;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 20, 2017
 */
@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {RepositoryConfiguration.class})
public class DAOUserTest {

    @Autowired
    private DAOUser userDAO;

    @Autowired
    private DAOJob jobDAO;

	@Autowired
    private PasswordEncoder passwordEncoder;
// 
        @Test
        public void findByAccount() throws Exception {
            User user = userDAO.findByAccount("ThaoPT");
            assertEquals("ThaoPT", user.getAccount());
        }

        @Test
        public void findAllByJob() throws Exception {
            java.util.List<User> users = userDAO.findAllByJob(jobDAO.getOne(2));
            assertEquals(2, users.size());
        }




}
