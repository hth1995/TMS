package com.hoang.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.User;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 22, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceUserTest {

    @Autowired
    private ServiceUser userService;
    @Autowired
    private ServiceJob jobService;

    @Autowired 
    private ServiceCourse courseService;

    @Autowired 
    private PasswordEncoder passwordEncoder;


    @Test
    public void getAllUser() throws Exception {
        List<User> users = userService.lietKe();
        assertEquals(5, users.size());
    }

    @Test
    public void getAllUserByJob() throws Exception {
   
        java.util.List<User> user = userService.findAllByJob(jobService.findOneSV(1));
        assertEquals(1, user.size());
    }

    @Test
    public void addUser() throws Exception {
        
        User user = new User();
        user.setAccount("Animal");
        user.setPassword("123");
    
        userService.them(user);
        User staff = userService.findOneSV(user.getUserId());
        assertNotNull(staff);
    }

    

    @Test
    public void findOneSV() throws Exception {
        User user = userService.findOneSV(1);
        assertNotNull(user);
    }

    @Test
    public void updateUser() throws Exception {
        User user = userService.findOneSV(4);
        user.setPassword("ABC");
        userService.sua(user);
        User user1 = userService.findOneSV(4);
        assertEquals(passwordEncoder.encode("ABC"),user1.getPassword());
    }

    @Test
    public void saveUser() throws Exception {
        User user = new User();
        user.setAccount("Animal");
        user.setPassword("123");
        userService.save(user);
        User user1 = userService.findOneSV(user.getUserId());
        assertNotNull(user1);
    }

  
    @Test
    public void deleteUser() throws Exception {
        userService.xoa(5);
        User user = userService.findOneSV(5);
        assertNull(user);
    }

    
   
    @Test
    public void findByAccount() throws Exception {
       
    User user = userService.findByAccount("Roots");
        assertEquals("Roots", user.getAccount());
    }

  

}
