package com.hoang.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Category;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 21, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceCategoryTest {
    @Autowired
    private ServiceCategory categoryService;
  
    @Test
    public void addCategory() throws Exception {
        Category category = new Category();
        category.setCategoryName("Leaf");
        category.setDescription("abcdefghk");
        
        
        categoryService.them(category);
        Category actualCategory = categoryService.findOneSV(category.getCategoryId());
        assertNotNull(actualCategory);
    }
    
    @Test
    public void deleteCategory() throws Exception {
        @SuppressWarnings("unused")
		Category category  = categoryService.findOneSV(22);
        categoryService.xoa(22);
        assertNull(categoryService.findOneSV(22));
    }

    @Test
    public void updateCategory() throws Exception {
        Category category  = categoryService.findOneSV(1);
        category.setCategoryName("ABC");
        categoryService.sua(category);
        Category categoryUpdated  = categoryService.findOneSV(1);
        assertEquals("ABC",categoryUpdated.getCategoryName());
    }
    
    @Test
    public void findOneCategory() throws Exception {
        Category category = categoryService.findOneSV(1);
        assertNotNull(category);
    }
    
    @Test
    public void getListCategoryActive() throws Exception {
        java.util.List<Category> categories = categoryService.lietKe();
        assertEquals(6, categories.size());
    }
    
}
