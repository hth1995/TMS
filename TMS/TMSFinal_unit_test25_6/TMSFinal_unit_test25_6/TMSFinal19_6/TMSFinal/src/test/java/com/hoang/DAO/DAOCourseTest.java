package com.hoang.DAO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Course;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 20, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {RepositoryConfiguration.class})
public class DAOCourseTest {
 
    @Autowired
    private DAOCourse courseDAO;
    @Autowired
    private DAOTopic topicDAO;
    @Autowired
    private DAOCategory cateDAO;
    @Test
    public void test() {
        Course course = new Course();
        course.setCourseName("Roots");
        course.setDescription("Learning english about Roots");
        course.setCategory(cateDAO.findOne(45));
        assertNull(course.getCourseId());
        courseDAO.save(course);
        assertNotNull(course.getCourseId());

        Course fetchedCourse = courseDAO.findOne(course.getCourseId());
        assertNotNull(fetchedCourse);
        assertEquals(course.getCourseId(), fetchedCourse.getCourseId());
        assertEquals(course.getCourseName(), fetchedCourse.getCourseName());
        assertEquals(course.getDescription(), fetchedCourse.getDescription());

        fetchedCourse.setDescription("Learn english about Roots");
        courseDAO.save(fetchedCourse);
        Course fetchedUpdatedCourse = courseDAO.findOne(fetchedCourse.getCourseId());
        assertEquals(fetchedCourse.getDescription(), fetchedUpdatedCourse.getDescription());

        long courseCount = courseDAO.count();
        assertEquals(courseCount, 5);

        Iterable<Course> courses = courseDAO.findAll();
        int count = 0;
        for(@SuppressWarnings("unused") Course c : courses){
            count++;
        }
        assertEquals(count, 5);

        
        courseDAO.save(fetchedUpdatedCourse);
        @SuppressWarnings("unused")
		Course fetchedDeletedCourse = courseDAO.findOne(fetchedCourse.getCourseId());
       
    }

    @Test
    public void findByCourseName() throws Exception {
        Course course = courseDAO.findByCourseName("66");
        assertEquals("66", course.getCourseName());
    }

@Test
    
    public void  findAllByTopics() throws Exception {
        java.util.List<Course> course = courseDAO.findAllByTopics(topicDAO.getOne(12));
        assertEquals(1, course.size());
    }

}
