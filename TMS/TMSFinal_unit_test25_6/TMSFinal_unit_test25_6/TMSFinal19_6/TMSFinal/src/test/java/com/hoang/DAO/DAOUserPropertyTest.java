package com.hoang.DAO;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Userproperty;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 21, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {RepositoryConfiguration.class})
public class DAOUserPropertyTest {
    @Autowired
    private DAOUserProperty userPropertyDAO;

    @Autowired
    private DAOUser userDAO;

    @Autowired
    private DAOProperty propertyDAO;
    @Autowired
    private DAOJob jobDAO;
    
    @Test
    public void findAllByUserAccount() throws Exception {
        java.util.List<Userproperty> user = userPropertyDAO.findAllByUser(userDAO.getOne(67));
        assertEquals(6, user.size());
    }
    
    @Test
    public void findAllByUserJob() throws Exception {
        java.util.List<Userproperty> user = userPropertyDAO.findAllByUserJob(jobDAO.getOne(67));
        assertEquals(11, user.size());
    } 
    @Test
    public void findAllByUser() throws Exception {
        java.util.List<Userproperty> us = userPropertyDAO.findAllByUser(userDAO.getOne(67));
        assertEquals(6, us.size());
    } 
    
//    @Test
//    public void findAllByIdPropertyIdAndIdUserId() throws Exception {
//        Userproperty userPropertyActual = userPropertyDAO.findAllByIdPropertyIdAndIdUserId(propertyDAO.getOne(1),userDAO.getOne(3));
//        assertEquals(1, propertyDAO.size());
//    }

}
