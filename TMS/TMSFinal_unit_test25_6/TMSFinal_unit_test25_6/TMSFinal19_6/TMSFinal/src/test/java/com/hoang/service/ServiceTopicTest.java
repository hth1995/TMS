package com.hoang.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Course;
import com.hoang.domain.Topic;
/**
 * @author XUKAKULL
 * @version 1.0 Jun 22, 2017
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTopicTest {

    @Autowired
    private ServiceTopic topicService;

    @Autowired
    private ServiceUser userService;

  

    @Test
    public void getAllTopic() throws Exception {
        List<Topic> topics = topicService.lietKe();
        assertEquals(4, topics.size());
    }


    @Test
    public void finOneById() throws Exception {
        Topic topic = topicService.findById(3);
        assertNotNull(topic);
    }

    @Test
    public void findAllByUserAccount() throws Exception {
       
    java.util.List<Topic> topic = topicService.findAllByUserAccount("admin");
        assertEquals(1, topic.size());
    }
    

   

    @Test
    public void findAllTopicByCourse() throws Exception {
        
       Course course = new Course();
       int id = course.getCourseId();
       List<Topic> topics = topicService.findAllByCourseCourseId(id);
       assertEquals(1, topics.size());
    }

    @Test
    public void findAllbyUserName() throws Exception {
        List<Topic> topics = topicService.findAllByUsername(userService.findOneSV(4));
        assertEquals(2, topics.size());
    }
    
    @Test
    public void updateTopic() throws Exception {
        Topic topic = topicService.findOneSV(10);
        topic.setTopicName("Roots Basics");
        topicService.sua(topic);
        Topic topic1 = topicService.findById(1);
        assertEquals("Roots Basics", topic1.getTopicName());
    }

    @Test
    public void deleteTopic() throws Exception {
        topicService.xoa(2);
        Topic topic = topicService.findById(1);
        assertNull(topic);
    }

    @Test
    public void addTopic() throws Exception {
        Topic topic = new Topic();
        topic.setTopicName("Question about Roots");
        topic.setDescription("ABC");
        topicService.them(topic);
        int id = topic.getTopicId();
        Topic topic1 = topicService.findById(id);
        assertEquals("Question about Roots", topic1.getTopicName());
    }

   

    
}