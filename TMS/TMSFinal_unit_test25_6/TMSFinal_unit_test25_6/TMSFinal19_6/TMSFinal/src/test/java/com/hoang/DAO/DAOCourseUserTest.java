package com.hoang.DAO;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.CourseUser;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 21, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {RepositoryConfiguration.class})
public class DAOCourseUserTest {
    @Autowired
    private DAOCourseUser courseUserDAO;
    @Test
    public void findAllByIdCourseIdAndIdUserId() throws Exception {
        CourseUser courseUser = courseUserDAO.findAllByIdCourseIdAndIdUserId(10,56);
        assertEquals(56, courseUser.getId());
    }

}
