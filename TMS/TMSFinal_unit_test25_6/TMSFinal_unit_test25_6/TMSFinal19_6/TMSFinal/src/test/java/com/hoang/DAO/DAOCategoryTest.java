package com.hoang.DAO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hoang.domain.Category;

/**
 * @author XUKAKULL
 * @version 1.0 Jun 20, 2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {RepositoryConfiguration.class})
public class DAOCategoryTest {
  
    @Autowired
    private DAOCategory cateDAO;

    @Test
    public void test() {
        Category cate = new Category();
        cate.setCategoryName("Animal");
        cate.setDescription("Description about animal");
        assertNull(cate.getCategoryId());
        cateDAO.save(cate);
        assertNotNull(cate.getCategoryId());
        
        Category fetchedCate = cateDAO.findOne(cate.getCategoryId());
        assertNotNull(fetchedCate);
        assertEquals(cate.getCategoryId(), fetchedCate.getCategoryId());
        assertEquals(cate.getCategoryName(), fetchedCate.getCategoryName());
        assertEquals(cate.getDescription(), fetchedCate.getDescription());
        
        fetchedCate.setDescription("Description about animal");
        cateDAO.save(fetchedCate);
        Category fetchedUpdatedCate = cateDAO.findOne(fetchedCate.getCategoryId());
        assertEquals(fetchedCate.getDescription(), fetchedUpdatedCate.getDescription());
        
        long categoryCount = cateDAO.count();
        assertEquals(categoryCount, 4);
        
        Iterable<Category> categories = cateDAO.findAll();
        int count = 0;
        for(@SuppressWarnings("unused") Category c : categories){
            count++;
        }
        assertEquals(count, 4);
        
        cateDAO.save(fetchedUpdatedCate);
        @SuppressWarnings("unused")
		Category fetchedDeletedCate = cateDAO.findOne(fetchedUpdatedCate.getCategoryId());
        
    }

}
